using System.Collections.Generic;
using Sharpmake;

[Generate]
class ProjectBase : Project
{
    public ProjectBase()
    {
        RootPath = @"[project.SharpmakeCsPath]\..";
        AddTargets(Config.DefaultTarget);
    }

    [Configure]
    public virtual void ConfigureAll(Project.Configuration conf, Target target)
    {
        conf.ProjectPath        = @"[project.RootPath]\projects";
        conf.IntermediatePath   = @"[project.RootPath]\projects\obj\[target.Platform]_[target.Optimization]\[project.Name]";
        conf.TargetPath         = @"[project.RootPath]\bin\[target.Platform]_[target.Optimization]";

        conf.Options.AddRange(new List<object>()
        {
            // Solution version
            Options.Vc.General.WindowsTargetPlatformVersion.v10_0_18362_0,
            Options.Vc.General.PlatformToolset.v142,
            // Warnings
            Options.Vc.General.WarningLevel.Level3,
            Options.Vc.General.TreatWarningsAsErrors.Enable,
            // Compiler flags
            Options.Vc.Compiler.Exceptions.Enable,
            Options.Vc.Compiler.CppLanguageStandard.CPP17,
        });

        conf.Defines.AddRange(new List<string>()
        {
            "_SILENCE_CXX17_OLD_ALLOCATOR_MEMBERS_DEPRECATION_WARNING",
            "NOMINMAX"
        });

        if (target.Optimization == Optimization.Debug)
        {
            conf.Defines.Add("_ITERATOR_DEBUG_LEVEL=0"); // bgfx uses iterator debug level 0 in debug mode.

            conf.Options.AddRange(new List<object>()
            {
                // Edit and continue
                Options.Vc.General.DebugInformation.ProgramDatabaseEnC,
                Options.Vc.Compiler.FunctionLevelLinking.Enable,
                Options.Vc.Linker.Incremental.Enable
            });
        }
    }
}

[Generate]
class GameMain : ProjectBase
{
    public GameMain()
    {
        SourceRootPath = @"[project.RootPath]\source\gamemain";
    }

    [Configure]
    public override void ConfigureAll(Project.Configuration conf, Target target)
    {
        base.ConfigureAll(conf, target);

        conf.AddPublicDependency<Engine>(target);

        conf.Output = Configuration.OutputType.Exe;

        // Set working directory
        conf.VcxprojUserFile = new Configuration.VcxprojUserFileSettings();
        conf.VcxprojUserFile.OverwriteExistingFile = true;
        conf.VcxprojUserFile.LocalDebuggerWorkingDirectory = @"$(TargetDir)";
    }
}

[Generate]
class GreenSln : Solution
{
    public GreenSln()
    {
        Name = "Green";
        AddTargets(Config.DefaultTarget);
    }

    [Configure]
    public void ConfigureAll(Solution.Configuration conf, Target target)
    {
        conf.SolutionPath = @"[solution.SharpmakeCsPath]\..\projects";

        conf.AddProject<GameMain>(target);
        conf.AddProject<Editor>(target);
        conf.AddProject<Test>(target);

        var editorProject = conf.GetProject(typeof(Editor));
        var gameMainProject = conf.GetProject(typeof(GameMain));
    }
}

class Config
{
    public static readonly DevEnv VsVersion = DevEnv.vs2019;

    public static readonly Target DefaultTarget     = new Target(Platform.win64, VsVersion, Optimization.Debug | Optimization.Release, OutputType.Lib, Blob.NoBlob, BuildSystem.MSBuild, DotNetFramework.v4_7_2);

    [Main]
    public static void SharpmakeMain(Arguments sharpmakeArgs)
    {
        sharpmakeArgs.Generate<GreenSln>();
    }
}
