﻿using System.Collections.Generic;
using Sharpmake;

[Generate]
class Editor : CSharpProject
{
    public Editor()
    {
        RootPath = @"[project.SharpmakeCsPath]\..\";
        SourceRootPath = @"[project.RootPath]\source\editor";

        AddTargets(Config.DefaultTarget);
    }

    [Configure]
    public void ConfigureAll(Project.Configuration conf, Target target)
    {
        conf.ProjectPath = @"[project.RootPath]\projects";
        conf.IntermediatePath = @"[project.RootPath]\projects\obj\[target.Platform]_[target.Optimization]\[project.Name]";
        conf.TargetPath = @"[project.RootPath]\bin\[target.Platform]_[target.Optimization]";

        conf.Output = Configuration.OutputType.DotNetWindowsApp;

        conf.Options.AddRange(new List<object>()
    {
        Options.CSharp.AutoGenerateBindingRedirects.Enabled,
        Options.Vc.General.TreatWarningsAsErrors.Enable,
    });

        conf.ReferencesByName.AddRange(new List<string>()
    {
        "System",
        "System.Core",
        "System.Data",
        "System.Linq",
        "System.Threading",
        "System.Windows",
        "System.Xaml",
        "WindowsBase",
        "PresentationCore",
        "PresentationFramework",
    });

        var EnvDTEReference = new DotNetReference("EnvDTE", DotNetReference.ReferenceType.DotNet);
        EnvDTEReference.EmbedInteropTypes = true; // Required for this reference to load correctly
        conf.DotNetReferences.Add(EnvDTEReference);

        conf.AddPublicDependency<GameMain>(target, DependencySetting.OnlyBuildOrder); // Ensure game/engine changes are always built when editor is built
    }
}
