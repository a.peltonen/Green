﻿using System.Collections.Generic;
using Sharpmake;

[Generate]
class EngineProject : ProjectBase
{
    public EngineProject()
    {
    }

    [Configure]
    public override void ConfigureAll(Project.Configuration conf, Target target)
    {
        base.ConfigureAll(conf, target);

        conf.IncludePaths.Add(@"[project.RootPath]\source");
    }
}

//////////////////////////////////////////////////////////////////

[Generate]
class Engine : EngineProject
{
    public Engine()
    {
        SourceRootPath = @"[project.RootPath]\source\engine";

        SourceFilesExtensions.Add(".sc"); // Shader sources
        SourceFilesExtensions.Add(".sh"); // Shader headers
        SourceFilesExcludeRegex.Add(".*.bin.h"); // Shader binaries
    }

    [Configure]
    public override void ConfigureAll(Project.Configuration conf, Target target)
    {
        base.ConfigureAll(conf, target);
        conf.Output = Configuration.OutputType.Lib;

        // Shader compilation build step
        conf.CustomBuildStep.Add(@"$(SolutionDir)/../compile_shaders.bat");
        conf.CustomBuildStepDescription = "=== Compile shaders ===";
        conf.CustomBuildStepOutputs.Add("non-existing-file-to-force-build-step.txt");
        conf.CustomBuildStepTreatOutputAsContent = "No";
        conf.CustomBuildStepBeforeTargets = "PreBuildEvent";

        conf.AddPublicDependency<Bgfx>(target);
        conf.AddPublicDependency<MathGeoLib>(target);
        conf.AddPublicDependency<Imgui>(target);
        conf.AddPublicDependency<Assimp>(target);
        conf.AddPublicDependency<Fmt>(target);
    }
}
