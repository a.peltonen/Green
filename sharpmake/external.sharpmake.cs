﻿using System.Collections.Generic;
using Sharpmake;

[Generate]
class ExternalProject : ProjectBase
{
    public ExternalProject()
    {
    }

    [Configure]
    public override void ConfigureAll(Project.Configuration conf, Target target)
    {
        base.ConfigureAll(conf, target);
        conf.Output = Configuration.OutputType.Lib;
        conf.SolutionFolder = "external";
        conf.IncludePaths.Add(@"[project.RootPath]\external");
    }
}

//////////////////////////////////////////////////////////////////

[Generate]
class Bgfx : ExternalProject
{
    public Bgfx()
    {
        SourceRootPath = @"[project.RootPath]\external\bgfx";
    }

    [Configure]
    public override void ConfigureAll(Project.Configuration conf, Target target)
    {
        base.ConfigureAll(conf, target);

        conf.IncludePaths.Add(@"[project.RootPath]\external\bgfx\include");
        conf.IncludePaths.Add(@"[project.RootPath]\external\bgfx\include\compat\msvc");

        string libSuffix = target.Optimization == Optimization.Debug ? "Debug" : "Release";
        conf.LibraryFiles.Add(string.Concat(@"[project.RootPath]\external\bgfx\lib\bgfx", libSuffix, ".lib"));
        conf.LibraryFiles.Add(string.Concat(@"[project.RootPath]\external\bgfx\lib\bx", libSuffix, ".lib"));
        conf.LibraryFiles.Add(string.Concat(@"[project.RootPath]\external\bgfx\lib\example-common", libSuffix, ".lib"));
        conf.LibraryFiles.Add(string.Concat(@"[project.RootPath]\external\bgfx\lib\bimg", libSuffix, ".lib"));
        conf.LibraryFiles.Add(string.Concat(@"[project.RootPath]\external\bgfx\lib\bimg_decode", libSuffix, ".lib"));
        conf.LibraryFiles.Add(string.Concat(@"[project.RootPath]\external\bgfx\lib\bimg_encode", libSuffix, ".lib"));
    }
}

[Generate]
class MathGeoLib : ExternalProject
{
    public MathGeoLib()
    {
        SourceRootPath = @"[project.RootPath]\external\mathgeolib";
    }

    [Configure]
    public override void ConfigureAll(Project.Configuration conf, Target target)
    {
        base.ConfigureAll(conf, target);

        conf.Defines.AddRange(new List<string>()
        {
            "_CRT_SECURE_NO_WARNINGS", // warning C4996: 'sprintf': This function or variable may be unsafe.
            "WIN32"
        });
    }
}

[Generate]
class Imgui : ExternalProject
{
    public Imgui()
    {
        SourceRootPath = @"[project.RootPath]\external\imgui";
        SourceFilesExcludeRegex.Add(@"\\examples\\", @"\\misc\\");
    }

    [Configure]
    public override void ConfigureAll(Project.Configuration conf, Target target)
    {
        base.ConfigureAll(conf, target);

        conf.IncludePaths.Add(@"[project.RootPath]\external\imgui");

    }
}

[Export]
class Assimp : ExternalProject
{
    public Assimp()
    {
        SourceRootPath = @"[project.RootPath]\external\assimp-5.0.1";
    }

    [Configure]
    public override void ConfigureAll(Project.Configuration conf, Target target)
    {
        base.ConfigureAll(conf, target);

        conf.Output = Configuration.OutputType.Lib;

        conf.IncludePaths.Add(@"[project.SourceRootPath]\include");

        if (target.Optimization == Optimization.Debug)
        {
            conf.LibraryPaths.Add(@"[project.SourceRootPath]\lib\Debug");
            conf.TargetFileName = "assimp-vc142-mtd.lib;IrrXMLd.lib;zlibstaticd.lib";
        }
        else
        {
            conf.LibraryPaths.Add(@"[project.SourceRootPath]\lib\Release");
            conf.TargetFileName = "assimp-vc142-mt.lib;IrrXML.lib;zlibstatic.lib";
        }
    }
}

[Generate]
class Fmt : ExternalProject
{
    public Fmt()
    {
        SourceRootPath = @"[project.RootPath]\external\fmt-7.1.0\src";
    }

    [Configure]
    public override void ConfigureAll(Project.Configuration conf, Target target)
    {
        base.ConfigureAll(conf, target);

        conf.IncludePaths.Add(@"[project.RootPath]\external\fmt-7.1.0\include");
    }
}
