using System.Collections.Generic;
using Sharpmake;

[Generate]
class Test : ProjectBase
{
    public Test()
    {
        SourceRootPath = @"[project.RootPath]\source\test";

        SourceFilesExtensions.Add(".sc"); // Shader sources
        SourceFilesExtensions.Add(".sh"); // Shader headers
        SourceFilesExcludeRegex.Add(".*.bin.h"); // Shader binaries
    }

    [Configure]
    public override void ConfigureAll(Project.Configuration conf, Target target)
    {
        base.ConfigureAll(conf, target);

        conf.IncludePaths.Add(@"[project.RootPath]\source");
        conf.Output = Configuration.OutputType.Exe;

        conf.AddPublicDependency<Engine>(target);

        // Disable warnings from catch2 macros
        conf.Options.Add(new Options.Vc.Compiler.DisableSpecificWarnings(
            "6319", // C6319: use of the comma-operator in a tested expression causes the left argument to be ignored when it has no side-effects
            "6237"  // C6237: (<zero> && <expression>) is always zero. <expression> is never evaluated and may have side effects
        ));
    }
}
