@echo off

set SHARPMAKE_ROOT=./external/Sharpmake
set SHARPMAKE_EXE=%SHARPMAKE_ROOT%/bin/release/Sharpmake.Application.exe

if not exist "%SHARPMAKE_EXE%" (
    Pushd
    echo Building Sharpmake...
    echo.
    CALL %SHARPMAKE_ROOT%/CompileSharpmake.bat Sharpmake.Application/Sharpmake.Application.csproj Release AnyCPU    
    Popd
)

echo Generating Solutions...
echo.
"%SHARPMAKE_EXE%" /sources(@"sharpmake/main.sharpmake.cs", @"sharpmake/engine.sharpmake.cs", @"sharpmake/editor.sharpmake.cs", @"sharpmake/external.sharpmake.cs" , @"sharpmake/test.sharpmake.cs") /verbose /generateDebugSolution