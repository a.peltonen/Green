﻿using System;
using System.Diagnostics;
using System.Text;
using System.Windows;
using System.Windows.Interop;

namespace Editor.Infrastructure
{
    public class EngineSession
    {
        public Window WindowHost;
        public IntPtr WindowHandle;
        public HwndHost HwndHost;
        public IntPtr Parent;
        public Process Process;

        private const string ProcessName = "gamemain";

        private EngineJobObject jobObject;

        public EngineSession()
        {
            Process = Process.Start(ProcessName);

            // If debugging, attach debugger to engine as well
            if (Debugger.IsAttached)
            {
                Process visualStudioProcess = null;
                do
                {
                    visualStudioProcess = DebuggerAttacher.GetAttachedVisualStudio(Process.GetCurrentProcess());
                } while (visualStudioProcess == null);
                DebuggerAttacher.AttachVisualStudioToProcess(visualStudioProcess, Process);
            }
            else
            {
                // HACK: Without debugger, someting in the the window hosting sequence goes too fast and the
                // hosted window isn't initially properly resized. This sleep seems to fix that.
                System.Threading.Thread.Sleep(400);
            }

            // Spin until one of the engine process windows have a title that matches the exe name,
            // that is the engine main window.
            // TODO: Use IPC to get the window handle from engine when it has finished launching
            while (WindowHandle == IntPtr.Zero)
            {
                foreach (ProcessThread thread in Process.Threads)
                {
                    Win32.User32.EnumThreadWindows((uint)thread.Id, new Win32.User32.EnumThreadDelegate(GetThreadWindowTitle), IntPtr.Zero);
                }
            }
            
            // Assign engine as child process to editor, so it is killed when editor process dies
            jobObject = new EngineJobObject();
            jobObject.AddProcess(Process.Handle);

            // Make sure Editor is on top
            Process editor = Process.GetCurrentProcess();
            IntPtr editorWindowHandle = editor.MainWindowHandle;
            Trace.Assert(editorWindowHandle != IntPtr.Zero);
            Win32.User32.SetForegroundWindow(editorWindowHandle);
        }

        public bool GetThreadWindowTitle(IntPtr hWnd, IntPtr lParam)
        {
            StringBuilder windowTitle = new StringBuilder(128);
            Win32.User32.GetWindowText(hWnd, windowTitle, windowTitle.Capacity);
            
            if (windowTitle.ToString() == ProcessName)
            {
                WindowHandle = hWnd;
            }
            return true;
        }

        public void Dispose()
        {
            if (!Process.HasExited)
                Process.Kill();

            jobObject.Dispose();
        }
    }
}
