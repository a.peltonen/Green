﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Diagnostics;
using System.Threading;
using Editor.ViewModels;

namespace Editor.Infrastructure
{
    public partial class MainWindow : Window
    {
        private EngineSession _engineSession;
        private Thread _engineLoadingthread;
        private MainWindowViewModel _viewModel;

        private bool _isDisposed;

        public MainWindow()
        {
            InitializeComponent();

            _viewModel = new MainWindowViewModel();
            DataContext = _viewModel;

            Closed += new EventHandler(OnViewportClose);
        }

        /// <summary>
        /// Dispose engine session on close.
        /// </summary>
        public void Dispose()
        {
            if (_isDisposed)
                return;

            _engineSession.Dispose();
            _engineSession = null;

            _isDisposed = true;
        }

        /// <summary>
        /// Called when the size of the viewport changes.
        /// </summary>
        private void OnHostSizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (_engineSession != null && _engineSession.HwndHost != null)
            {
                _engineSession.HwndHost.UpdateWindowPos();
                _engineSession.HwndHost.UpdateLayout();
            }
        }

        /// <summary>
        /// Called when the viewport gets closed.
        /// </summary>
        void OnViewportClose(object sender, EventArgs e)
        {
            _engineSession.Dispose();
            _engineLoadingthread.Abort();
        }

        /// <summary>
        /// Called when the user control is initialized.
        /// </summary>
        private void OnLoaded(object sender, EventArgs e)
        {
            // Start engine loading in another thread
            _engineLoadingthread = new Thread(LoadEngineSession);
            _engineLoadingthread.SetApartmentState(System.Threading.ApartmentState.STA);
            _engineLoadingthread.Start();

            // Launch another thread to display the loading of the GAV
            new Thread(_viewModel.DisplayEngineLoadingProgress).Start();

            // Bind event control events.
            this.KeyUp += new KeyEventHandler(Viewport_KeyUp);
            this.KeyDown += new KeyEventHandler(Viewport_KeyDown);
            this.PreviewKeyDown += new KeyEventHandler(Viewport_PreviewKeyDown);
        }


        /// <summary>
        /// Launches the engine process and grabs its main window.
        /// </summary>
        private void LoadEngineSession()
        {
            // Create the hosted session.
            EngineSession session = new EngineSession();
            session.WindowHost = this;

            // Prepare the hosted control in the UI thread.
            Application.Current.Dispatcher.Invoke(() => CreateHostedControl(session));

            _viewModel.LoadingFinished();
        }


        /// <summary>
        /// Creates and show the host control.
        /// </summary>
        /// <remarks>Should only be called in the UI thread.</remarks>
        private void CreateHostedControl(EngineSession session)
        {
            Trace.Assert(Application.Current.Dispatcher.Thread == Thread.CurrentThread);

            // Dynamically create the host control.
            WindowHost newHostedControl = new WindowHost(session);
            newHostedControl.Focusable = true;
            newHostedControl.Focus();

            _engineSession = session;

            // Save the control host for the session.
            session.HwndHost = newHostedControl;

            // Embed the control.
            ControlHostElement.Child = newHostedControl;
            ControlHostElement.SizeChanged += new SizeChangedEventHandler(OnHostSizeChanged);
        }

        void Viewport_KeyUp(object sender, KeyEventArgs e)
        {
        }

        void Viewport_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
        }

        void Viewport_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
        }
    }
}