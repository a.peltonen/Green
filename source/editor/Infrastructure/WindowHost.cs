﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;

using static Editor.Win32.User32;
using static Editor.Win32.Kernel32;

namespace Editor.Infrastructure
{
    /// <summary>
    /// WPF control to embed the engine process main window.
    /// </summary>
    public class WindowHost : HwndHost
    {
        private EngineSession _hostedSession = null;
        private HookProc _hookProc = null;
        private IntPtr _hookId;

        /// <summary>
        /// Construct the engine viewport host.
        /// </summary>
        public WindowHost(EngineSession hostedSession)
        {
            Trace.Assert(hostedSession != null);
            _hostedSession = hostedSession;
        }

        /// <summary>
        /// Grabs the host process's main window.
        /// </summary>
        protected override HandleRef BuildWindowCore(HandleRef hwndParent)
        {
            // Set the client window style and new parent.
            // This step is necessary so we can manage that externally created window.
            SetParent(_hostedSession.WindowHandle, hwndParent.Handle);
            SetWindowLong(
                _hostedSession.WindowHandle, 
                GWL_STYLE,
                WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS);

            _hostedSession.Parent = hwndParent.Handle;

            // Make sure we have a window.
            Trace.Assert(_hostedSession.WindowHandle != IntPtr.Zero && IsWindow(_hostedSession.WindowHandle));

            // Install keyboard hook procedure
            IntPtr moduleHandle = GetModuleHandle(_hostedSession.Process.MainModule.ModuleName);
            _hookProc = new HookProc(KeyboardProc);
            _hookId = SetWindowsHookEx(HookType.WH_KEYBOARD_LL, _hookProc, moduleHandle, 0);
            return new HandleRef(this, _hostedSession.WindowHandle);
        }

        /// <summary>
        /// Clean up any resources that the hosted window might have created.
        /// </summary>
        protected override void DestroyWindowCore(HandleRef hwnd)
        {
            UnhookWindowsHookEx(_hookId);
        }

        private int KeyboardProc(int code, IntPtr wParam, IntPtr lParam)
        {
            if (code < 0)
            {
                return (int)CallNextHookEx(IntPtr.Zero, (int)code, wParam, lParam);
            }

            KBDLLHOOKSTRUCT kbd = (KBDLLHOOKSTRUCT)Marshal.PtrToStructure(lParam, typeof(KBDLLHOOKSTRUCT));

            IntPtr foregroundWindow = GetForegroundWindow();
            if (foregroundWindow == _hostedSession.Parent)
            {
                PresentationSource source = PresentationSource.FromVisual(this);
                KeyEventArgs e = new KeyEventArgs(Keyboard.PrimaryDevice, source, 0, (Key)kbd.vkCode);
                e.RoutedEvent = Keyboard.KeyUpEvent;
                _hostedSession.WindowHost.RaiseEvent(e);

                Trace.WriteLine(string.Format("TODO: InputManager.ProcessInput({0})", kbd.vkCode));
            }

            return (int)CallNextHookEx(IntPtr.Zero, (int)code, wParam, lParam);
        }
    }
}
