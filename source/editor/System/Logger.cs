﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;

namespace Editor
{
    class Logger
    {
        public static void Log(
            string msg,
            [CallerLineNumber] int lineNumber = 0,
            [CallerFilePath] string filePath = "",
            [CallerMemberName] string caller = null)
        {
            WriteWithPrefix("", msg, lineNumber, filePath, caller);
        }

        public static void LogWarning(
            string msg,
            [CallerLineNumber] int lineNumber = 0,
            [CallerFilePath] string filePath = "",
            [CallerMemberName] string caller = null)
        {
            WriteWithPrefix("Warning: ", msg, lineNumber, filePath, caller);
        }

        public static void LogError(
          string msg,
          [CallerLineNumber] int lineNumber = 0,
          [CallerFilePath] string filePath = "",
          [CallerMemberName] string caller = null)
        {
            WriteWithPrefix("Error: ", msg, lineNumber, filePath, caller);
        }

        private static void WriteWithPrefix(
            string prefix,
            string msg,
            int lineNumber,
            string filePath,
            string caller)
        {
            Trace.WriteLine($"{prefix}{Path.GetFileName(filePath)}:{lineNumber}: {msg}");
        }
    }
}
