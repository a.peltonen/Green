#include <engine/ecs/Database.hpp>

namespace green
{

	Database::Database(const Registry& registry)
		: m_registry(registry)
	{
	}

	Id Database::createEntity()
	{
		return m_idGen.generate();
	}

	std::vector<Id> Database::allEntitiesTemp() const
	{
		std::vector<Id> ids;
		for (auto&& st : m_storage)
		{
			auto&& table = st.second;
			for (auto&& c : table)
			{
				ids.emplace_back(c.first);
			}
		}

		std::sort(ids.begin(), ids.end());
		ids.erase(std::unique(ids.begin(), ids.end()), ids.end());

		return ids;
	}
}
