#pragma once

#include <engine/system/Errors.hpp>
#include <engine/ecs/Component.hpp>
#include <engine/ecs/IdGenerator.hpp>
#include <engine/ecs/Registry.hpp>

#include <unordered_map>
#include <typeinfo>
#include <type_traits>

namespace green
{

	class Database
	{
	public:
		Database(const Registry& registry);

		// Non-copyable
		Database(const Database& other) = delete;
		Database& operator=(const Database& other) = delete;

		const Registry& registry() const { return m_registry; }

		Id createEntity();

		// Temporary, slow accessors
		// Returns the union of all ids in all registered tables
		std::vector<Id> allEntitiesTemp() const;

		template <typename T>
		std::vector<Id> allEntitiesWithComponentTemp() const
		{
			std::vector<Id> ids;


			if (!m_registry.isRegistered<T>())
			{
				GR_ASSERT(false, "Unregistered component type");
				return ids;
			}

			ComponentTypeId typeId = typeid(T).hash_code();
			auto& table = m_storage.at(typeId);
			for (auto&& c : table)
			{
				ids.emplace_back(c.first);
			}

			std::sort(ids.begin(), ids.end());
			ids.erase(std::unique(ids.begin(), ids.end()), ids.end());

			return ids;
		}

		template <typename T>
		void assign(Id id, T&& component)
		{
			typedef std::decay<T>::type _T;

			if (!m_registry.isRegistered<T>())
			{
				GR_ASSERT(false, "Trying to assign component %s that is not registered", typeid(T).name());
				return;
			}

			ComponentTypeId typeId = typeid(T).hash_code();
			auto& table = m_storage[typeId];
			table[id] = std::unique_ptr<_T>(new _T(std::forward<T>(component)));
		}

		template <typename T>
		void remove(Id id)
		{
			if (!m_registry.isRegistered<T>())
			{
				GR_ASSERT(false, "Trying to remove component %s that is not registered", typeid(T).name());
				return;
			}

			ComponentTypeId typeId = typeid(T).hash_code();
			auto& table = m_storage[typeId];
			table.erase(id);
		}

		template <typename T>
		T* getComponent(Id id) 
		{
			if (!m_registry.isRegistered<T>())
			{
				GR_ASSERT(false, "Trying to get component %s that is not registered", typeid(T).name());
				return nullptr;
			}

			ComponentTypeId typeId = typeid(T).hash_code();
			auto& table = m_storage[typeId];
			return table.find(id) != table.end() ? static_cast<T*>(table[id].get()) : nullptr;
		}

		template <typename T>
		const T* getComponent(Id id) const
		{
			return const_cast<Database*>(this)->getComponent<T>(id);
		}
	private:

		std::unordered_map<ComponentTypeId, std::unordered_map<Id, std::unique_ptr<system::Component>>> m_storage;

		const Registry& m_registry;

		IdGenerator m_idGen;
	};
}