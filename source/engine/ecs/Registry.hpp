#pragma once

#include <engine/system/Errors.hpp>
#include <engine/ecs/Component.hpp>

#include <vector>
#include <algorithm>

namespace green
{
	using ComponentTypeId = uint64_t;

	class Registry
	{
	public:
		Registry() {}

		template <typename T>
		void registerComponentType()
		{
			ComponentTypeId typeId = typeid(T).hash_code();
			registerComponentType(typeId);
		}

		template <typename T>
		bool isRegistered() const
		{
			ComponentTypeId typeId = typeid(T).hash_code();
			return std::find(m_registeredTypes.begin(), m_registeredTypes.end(), typeId) != m_registeredTypes.end();
		}

	private:
		using ComponentTypeId = uint64_t;

		void registerComponentType(ComponentTypeId typeId)
		{
			for (auto& registered : m_registeredTypes)
			{
				GR_ASSERT(typeId != registered, "Tried to register the same component type twice");
			}

			m_registeredTypes.emplace_back(typeId);
		}

		std::vector<ComponentTypeId> m_registeredTypes;
	};
}