#pragma once

#include <engine/system/DataTypes.hpp>

namespace green
{
	class IdGenerator
	{
	public:
		Id generate();

	private:
		Id m_nextId = 0;
	};
}