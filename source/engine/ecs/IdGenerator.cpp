#include <engine/ecs/IdGenerator.hpp>

namespace green
{
	Id IdGenerator::generate()
	{
		return m_nextId++;
	}
}
