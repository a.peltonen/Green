#pragma once

#include <engine/system/DataTypes.hpp>
#include <engine/asset/Model.hpp>
#include <engine/asset/Material.hpp>

namespace green
{
	namespace system
	{
		struct Component
		{
			// TODO: Serialization
		};
	}


	// Some actual components
	// TODO: Move somewhere else

	struct Transform : public system::Component
	{
		Float3	translation		= Float3::zero;
		Qua		rotation		= Qua::identity; 
		float	scale			= 1.f;

		Transform() = default;

		Transform(Float3 t)
		{
			translation = t;
		}

		Transform(Float3 t, Qua r)
		{
			translation = t;
			rotation	= r;
		}

		Transform(Float3 t, Qua r, float s)
		{
			translation = t;
			rotation = r;
			scale = s;
		}

		bool operator==(const Transform& rhs) const
		{
			return translation.Equals(rhs.translation) &&
				rotation.Equals(rhs.rotation) &&
				scale == rhs.scale;
		}
	};

	// TODO: Consider if having pointers inside components is a good idea. Breaks const-correctness?
	struct ModelLink : public system::Component
	{
		std::shared_ptr<Model> modelData = nullptr;

		ModelLink() = default;

		ModelLink(std::shared_ptr<Model> model)
		{
			modelData = model;
		}
	};

	struct MaterialLink : public system::Component
	{
		std::shared_ptr<Material> materialData = nullptr;

		MaterialLink() = default;

		MaterialLink(std::shared_ptr<Material> material)
		{
			materialData = material;
		}
	};
}
