#pragma once

#include <engine/gfx/Common.hpp>

#include <optional>

namespace green
{
	class Material
	{

	public:
		enum class Type
		{
			Default = 0		// Default material type; 5-texture PBR for geometry rendering (albedo, normal, roughness, metalness, ao)
		};

		enum class ChannelsDefault
		{
			Albedo		= 0,
			Normal		= 1,
			Roughness	= 2,
			Metalness	= 3,
			AO			= 4
		};

		struct RuntimeData
		{
			bgfx::TextureHandle	textures[5];
		};

		struct ConstantOverrides
		{
			std::optional<Float3>	albedo;
			std::optional<float>	roughness;
			std::optional<float>	metalness;
		};

		Material();
		~Material();

		void	setSourceTextures(
			const fs::path& albedo,
			const fs::path& normal,
			const fs::path& roughness, 
			const fs::path& metalness, 
			const fs::path& ao);

		void						setConstantOverrides(const ConstantOverrides& overrides)	{ m_overrides = overrides; }
		const ConstantOverrides&	getConstantOverrides() const								{ return m_overrides; }

		void						load();
		void						unload();

		bool						loaded() const		{ return m_loaded; }
		const RuntimeData&			runtimeData() const	{ return m_runtimeData; }

	private:
		std::vector<fs::path>		m_sources;
		Type						m_type = Type::Default;
		ConstantOverrides			m_overrides;

		bool						m_loaded = false;
		RuntimeData					m_runtimeData;
	};
}