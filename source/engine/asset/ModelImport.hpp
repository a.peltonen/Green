#pragma once

#include <engine/system/DataTypes.hpp>
#include <random>

namespace green
{

	struct VertexData
	{
		Float3		position	= Float3(0, 0, 0);
		Float3		normal		= Float3(0, 1, 0);
		Float4		tangent		= Float4(1, 0, 0, 1);
		uint32_t	color		= 0xffffffff;
		Float2		texCoord	= Float2(0, 0);

		VertexData() 
		{
			color = rand();
		}
	};

	struct RawSubmodelData
	{
		std::vector<int32_t>		indices;
		std::vector<VertexData>		vertices;
	};

	struct RawModelData
	{
		std::vector<RawSubmodelData> submodels;
	};

	class ModelImport
	{
	public:
		static RawModelData import(const fs::path& path, float importScale);
	};
}