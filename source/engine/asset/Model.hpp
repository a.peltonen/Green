#pragma once

#include <engine/gfx/Common.hpp>
#include <engine/asset/ModelImport.hpp>

namespace green
{
	class Model
	{
		struct RuntimeSubmodelData
		{
			bgfx::VertexBufferHandle	vertexBuffer;
			bgfx::IndexBufferHandle		indexBuffer;
		};

		struct RuntimeModelData
		{
			std::vector<RuntimeSubmodelData> submodels;
		};

	public:
		Model(const fs::path& path);
		Model(const fs::path& path, float importScale);
		~Model();

		void					load();
		void					unload();

		const std::string&		name() const											{ return m_name; }
		bool					loaded() const											{ return m_loaded; }
		const RuntimeModelData&	runtimeData() const										{ return m_runtimeData; }

	private:
		fs::path								m_path = "";
		std::string								m_name = "";
		float									m_importScale = 1.f;

		RawModelData							m_rawData;

		bool									m_loaded = false;
		RuntimeModelData						m_runtimeData;
	};
}