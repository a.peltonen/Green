#include <engine/asset/Model.hpp>

#include <engine/gfx/RenderConstants.hpp>
#include <engine/gfx/utils/ImageUtils.hpp>
#include <engine/asset/ModelImport.hpp>

namespace green
{
	Model::Model(const fs::path& path)
		: Model(path, 1.f)
	{
	}

	Model::Model(const fs::path& path, float importScale)
		: m_path(path)
		, m_name(path.stem().string())
		, m_importScale(importScale)
	{
	}

	Model::~Model()
	{
		if (m_loaded)
			unload();
	}

	void Model::load()
	{
		if (m_loaded)
			return;

		m_rawData = ModelImport::import(m_path, m_importScale);
		
		m_runtimeData.submodels.clear();

		for (auto&& sm : m_rawData.submodels)
		{
			RuntimeSubmodelData runtimeSubmodel;

			runtimeSubmodel.indexBuffer	= bgfx::createIndexBuffer(bgfx::makeRef(sm.indices.data(), static_cast<uint32_t>(sm.indices.size() * sizeof(int32_t))), BGFX_BUFFER_INDEX32);
			runtimeSubmodel.vertexBuffer = bgfx::createVertexBuffer(bgfx::makeRef(sm.vertices.data(), static_cast<uint32_t>(sm.vertices.size() * sizeof(VertexData))), RenderConstants::VertexLayout.Model);

			m_runtimeData.submodels.emplace_back(runtimeSubmodel);
		}

		m_loaded = true;
	}

	void Model::unload()
	{
		if (!m_loaded)
			return;

		for (auto&& sm : m_runtimeData.submodels)
		{
			bgfx::destroy(sm.vertexBuffer);
			bgfx::destroy(sm.indexBuffer);
		}

		m_loaded = false;
	}
}
