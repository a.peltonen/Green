#include <engine/asset/ModelImport.hpp>
#include <engine/system/Logging.hpp>
#include <engine/system/Errors.hpp>

#include <filesystem>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/mesh.h>
#include <assimp/postprocess.h>

namespace green
{
	namespace 
	{
		Float4 packTangentAndBitangentSign(Float3 normal, Float3 tangent, Float3 bitangent)
		{
			Float3 orthogonalizedTangent = (tangent - normal * normal.Dot(tangent)).Normalized();
			float bitangentSign = (normal.Cross(tangent)).Dot(bitangent) < 0.0f ? -1.f : 1.f;

			return Float4(orthogonalizedTangent.x, orthogonalizedTangent.y, orthogonalizedTangent.z, bitangentSign);
		}

		void gatherModelNodesDepthFirst(aiNode* currentNode, std::vector<aiNode*>& nodes)
		{
			if (!currentNode)
				return;

			if (currentNode->mNumMeshes > 0)
				nodes.emplace_back(currentNode);

			for (uint32_t c = 0; c < currentNode->mNumChildren; c++)
				gatherModelNodesDepthFirst(currentNode->mChildren[c], nodes);
		}

		Float3 aiVec3ToFloat3(aiVector3D vec)
		{
			return Float3(vec.x, vec.y, vec.z);
		}

		Float2 aiVec3ToFloat2(aiVector3D vec)
		{
			return Float2(vec.x, vec.y);
		}

		uint32_t aiColor4ToUint(aiColor4D vec)
		{
			return 0xffffffff; // TODO
		}

		RawSubmodelData makeSubmodelData(
			const aiMesh*	mesh,
			float			importScale)
		{
			bool hasPositions	= mesh->HasPositions();
			bool hasNormals		= mesh->HasNormals();
			bool hasTangents	= mesh->HasTangentsAndBitangents();
			bool hasUVs			= mesh->HasTextureCoords(0);
			bool hasBones		= mesh->HasBones();

			RawSubmodelData sm;

			for (unsigned i = 0; i < mesh->mNumVertices; i++)
			{
				VertexData vertex;

				if (hasPositions)
				{
					vertex.position = aiVec3ToFloat3(mesh->mVertices[i]) * importScale;
				}

				static constexpr int NumUVChannels = 1;
				for (int x = 0; x < NumUVChannels; x++)
				{
					if (mesh->HasTextureCoords(x))
					{
						vertex.texCoord = aiVec3ToFloat2(mesh->mTextureCoords[x][i]);
					}
				}

				static constexpr int NumColorChannels = 1;
				for (unsigned x = 0; x < NumColorChannels; x++)
				{
					if (mesh->HasVertexColors(x))
					{
						vertex.color = aiColor4ToUint(mesh->mColors[x][i]);
					}
				}

				Float3 normal;
				if (hasNormals)
				{
					normal = aiVec3ToFloat3(mesh->mNormals[i]);
				}

				Float3 tangent;
				Float3 bitangent;
				if (hasTangents)
				{
					tangent = aiVec3ToFloat3(mesh->mTangents[i]);
					bitangent = aiVec3ToFloat3(mesh->mBitangents[i]);
				}

				// Normalize
				normal			= normal.Normalized();
				tangent			= tangent.Normalized();
				bitangent		= bitangent.Normalized();

				// Calculate handedness before making bitangent orthogonal, because that might affect handedness.
				float bitangentSign = (normal.Cross(tangent)).Dot(bitangent) < 0.0f ? -1.f : 1.f;

				// Make orthogonal
				bitangent = tangent.Cross(normal);
				tangent = normal.Cross(bitangent);

				// Renormalize to avoid length being over 1 due to floating-point errors
				tangent = tangent.Normalized();

				vertex.normal	= normal;
				vertex.tangent	= packTangentAndBitangentSign(normal, tangent, bitangent);

				sm.vertices.emplace_back(vertex);
			}

			for (unsigned i = 0; i < mesh->mNumFaces; i++)
			{
				const aiFace face = mesh->mFaces[i];

				if (face.mNumIndices != 3)
				{
					GR_ASSERT(false, "Expected 3 indices in primitive, got %d", face.mNumIndices);
					continue;
				}
								
				sm.indices.emplace_back(face.mIndices[0]);
				sm.indices.emplace_back(face.mIndices[1]);
				sm.indices.emplace_back(face.mIndices[2]);
			}

			return sm;
		}
	}

	RawModelData ModelImport::import(const fs::path& path, float importScale)
	{
		using namespace Assimp;

		Importer importer;

		const aiScene* scene = importer.ReadFile(path.string().c_str(),
			aiProcess_CalcTangentSpace |
			aiProcess_Triangulate |
			aiProcess_JoinIdenticalVertices |
			aiProcess_FlipUVs |
			aiProcess_SortByPType);

		std::vector<aiNode*> modelNodes;
		gatherModelNodesDepthFirst(scene->mRootNode, modelNodes);

		RawModelData m;

		for (uint32_t i = 0; i < modelNodes.size(); i++)
		{
			aiNode* modelNode = modelNodes[i];

			for (unsigned j = 0; j < modelNode->mNumMeshes; j++)
			{
				auto aiMesh = scene->mMeshes[modelNode->mMeshes[j]];

				RawSubmodelData sm = makeSubmodelData(aiMesh, importScale);

				m.submodels.emplace_back(sm);
			}
		}

		return m;
	}
}