#include <engine/asset/Material.hpp>

#include <engine/gfx/utils/ImageUtils.hpp>

#include <algorithm>

namespace green	
{
	Material::Material()
	{
		InitializeInvalidHandles(m_runtimeData.textures, ARRAY_SIZE(m_runtimeData.textures));
	}

	Material::~Material()
	{
		if (m_loaded)
			unload();
	}

	void Material::setSourceTextures(
		const fs::path& albedo, 
		const fs::path& normal,
		const fs::path& roughness, 
		const fs::path& metalness, 
		const fs::path& ao)
	{
		m_sources.emplace_back(albedo);
		m_sources.emplace_back(normal);
		m_sources.emplace_back(roughness);
		m_sources.emplace_back(metalness);
		m_sources.emplace_back(ao);
	}

	void Material::load()
	{
		int i = 0;
		for (auto&& sourcePath : m_sources)
		{
			m_runtimeData.textures[i++] = ImageUtils::loadTexture(sourcePath);
		}

		m_loaded = true;
	}

	void Material::unload()
	{
		for (int i = 0; i < ARRAY_SIZE(m_runtimeData.textures); i++)
		{
			if (bgfx::isValid(m_runtimeData.textures[i]))
			{
				bgfx::destroy(m_runtimeData.textures[i]);
			}
			m_runtimeData.textures[i] = BGFX_INVALID_HANDLE;
		}

		m_loaded = false;
	}
}
