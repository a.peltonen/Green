#include <engine/core/InputActions.hpp>
#include <bgfx/bgfx.h>

#include <imgui.h>

namespace green
{
	static std::unordered_set<InputAction> s_keydownActionsBuffer;
	void InputActions::onKeyPressed(const void* userData)
	{
		auto c = static_cast<const char*>(userData);
		if (c == "�") // TODO: Fix
		{
			s_keydownActionsBuffer.insert(InputAction::ToggleDebugMenu);
		}
	}

	InputActions::InputActions()
	{
		m_holdActions.emplace_back(HoldActionMapping { InputAction::CameraForward,		entry::Key::KeyW });
		m_holdActions.emplace_back(HoldActionMapping { InputAction::CameraBack,			entry::Key::KeyS });
		m_holdActions.emplace_back(HoldActionMapping { InputAction::CameraLeft,			entry::Key::KeyA });
		m_holdActions.emplace_back(HoldActionMapping { InputAction::CameraRight,		entry::Key::KeyD });

		m_keyDownActions = {
			{ entry::Key::Backslash, entry::Modifier::None, 1, onKeyPressed, "�" },

			INPUT_BINDING_END // TODO: Get rid of this ridiculous crap
		};

		// Add custom keybinds
		inputAddBindings("engine bindings", m_keyDownActions.data());
	}

	void InputActions::update()
	{
		m_activeActions.clear();
		m_activeActions.insert(s_keydownActionsBuffer.begin(), s_keydownActionsBuffer.end());
		s_keydownActionsBuffer.clear();

		for (auto&& binding : m_holdActions)
		{
			if (inputGetKeyState(binding.key))
			{
				m_activeActions.insert(binding.action);
			}
		}

		// TODO: Is mouse handling supposed to be here? Rename this class?
		float newMousePosition[3];
		inputGetMouse(newMousePosition);
		bool mouseMoved = newMousePosition[0] != 0 && newMousePosition[1] != 0;
		if (mouseMoved)
		{
			m_mouseNormalizedDelta.x = newMousePosition[0] - m_mouseNormalizedPos.x;
			m_mouseNormalizedDelta.y = newMousePosition[1] - m_mouseNormalizedPos.y;
			m_mouseNormalizedPos.x = newMousePosition[0];
			m_mouseNormalizedPos.y = newMousePosition[1];
		}
		else
		{
			m_mouseNormalizedDelta = Float2::zero;
		}

		// TODO: Encapsulate
		m_mouseLeftDown = inputGetMouseButtonState(entry::MouseButton::Left);
		m_mouseRightDown = inputGetMouseButtonState(entry::MouseButton::Right);
		m_mouseWheel = newMousePosition[2];
	}

	void InputActions::drawDebugMenu()
	{
		if (ImGui::CollapsingHeader("Input"))
		{
			ImGui::Columns(2, "inputcolumns", false);
			ImGui::Text("Mouse position (normalized)"); ImGui::NextColumn();
			ImGui::Text("%.3f, %.3f", m_mouseNormalizedPos.x, m_mouseNormalizedPos.y); ImGui::NextColumn();
			ImGui::Text("Mouse delta (normalized)"); ImGui::NextColumn();
			ImGui::Text("%.3f, %.3f", m_mouseNormalizedDelta.x, m_mouseNormalizedDelta.y); ImGui::NextColumn();
			ImGui::Text("Mouse buttons"); ImGui::NextColumn();
			ImGui::Text("L %d R %d", m_mouseLeftDown, m_mouseRightDown); ImGui::NextColumn();
			ImGui::Columns(1);
		}
	}
}
