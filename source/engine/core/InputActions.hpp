#pragma once

#include <engine/system/Errors.hpp>
#include <engine/core/Camera.hpp>
#include <engine/system/DataTypes.hpp>

#include <engine/bgfx_utils/entry/input.h>

#include <unordered_set>
#include <vector>

namespace green
{
	enum class InputAction
	{
		CameraForward,
		CameraLeft,
		CameraRight,
		CameraBack,

		ToggleDebugMenu
	};

	struct HoldActionMapping
	{
		InputAction			action;
		entry::Key::Enum	key;
	};

	class InputActions
	{
	public:
		InputActions();

		void update();

		bool isActive(InputAction action) const
		{
			return m_activeActions.find(action) != m_activeActions.end();
		}

		bool getMouseLeftDown() const { return m_mouseLeftDown; }
		bool getMouseRightDown() const { return m_mouseRightDown; }
		float getMouseWheel() const { return m_mouseWheel; }
		bool mouseMoved() const { return !m_mouseNormalizedDelta.Equals(Float2::zero); }
		Float2 getMouseNormalizedPos() const { return m_mouseNormalizedPos; }
		Float2 getMouseNormalizedDelta() const { return m_mouseNormalizedDelta; }

		void drawDebugMenu();

	private:
		static void						onKeyPressed(const void* userData);

		Float2							m_mouseNormalizedPos = Float2::zero;
		Float2							m_mouseNormalizedDelta = Float2::zero;
		bool							m_mouseLeftDown = false;
		bool							m_mouseRightDown = false;
		float							m_mouseWheel = 0.f;

		std::vector<HoldActionMapping>	m_holdActions;
		std::vector<InputBinding>		m_keyDownActions; // bgfx crap

		std::unordered_set<InputAction>	m_activeActions;
	};
}
