#include <engine/core/Camera.hpp>
#include <engine/core/InputActions.hpp>
#include <engine/scene/ICameraController.hpp>
#include <bgfx/bgfx.h>
#include <utility>
#include <imgui.h>

namespace green
{
	const Float3 Camera::UpVector = Float3(0.0f, 1.0f, 0.0f);

	Camera::Camera()
		: m_position(0.0f, 20.0f, -40.0f)
	{
		recomputeMatrices();
	}

	void Camera::setPosition(Float3 pos)
	{
		m_position = std::move(pos);
		recomputeMatrices();
	}

	void Camera::move(Float3 direction)
	{
		m_position = m_position + direction;
		recomputeMatrices();
	}

	void Camera::rotate(float yawDelta, float pitchDelta)
	{
		m_yaw += yawDelta;
		m_pitch += pitchDelta;

		static constexpr float epsilon = 1e-3;
		m_pitch = mgl::Clamp(m_pitch, -bx::kPiHalf + epsilon, bx::kPiHalf - epsilon);

		recomputeMatrices();
	}

	void Camera::setVerticalFov(float fov)
	{
		m_verticalFov = fov;
		recomputeMatrices();
	}

	void Camera::setViewportSize(Float2 size)
	{
		m_viewportSize = size;
		recomputeMatrices();
	}

	void Camera::setNearPlane(float nearPlane)
	{
		m_nearPlane = nearPlane;
		recomputeMatrices();
	}

	void Camera::setFarPlane(float farPlane)
	{
		m_farPlane = farPlane;
		recomputeMatrices();
	}

	Float3 Camera::getPosition() const
	{
		return m_position;
	}

	Float3 Camera::getForward() const
	{
		return m_viewMatrix.Row3(2).Normalized(); // Forward vector is third column of camera transform (inverse of view, ~transpose)
	} 

	Float3 Camera::getRight() const
	{
		return m_viewMatrix.Row3(0).Normalized(); // Right vector is first column of camera transform (inverse of view, ~transpose)
	}

	float Camera::getVerticalFov() const
	{
		return m_verticalFov;
	}

	Float2 Camera::getViewportSize() const
	{
		return m_viewportSize;
	}

	float Camera::getNearPlane() const
	{
		return m_nearPlane;
	}

	float Camera::getFarPlane() const
	{
		return m_farPlane;
	}

	const Mat& Camera::getViewMatrix() const
	{
		return m_viewMatrix;
	}

	const Mat& Camera::getProjectionMatrix() const
	{
		return m_projectionMatrix;
	}

	const Mat& Camera::getOrthographicMatrix() const
	{
		return m_orthographicMatrix;
	}

	void Camera::drawDebugMenu()
	{
		if (ImGui::CollapsingHeader("Camera"))
		{
			ImGui::Columns(2, "cameracolumns", false);
			ImGui::Text("Camera position"); ImGui::NextColumn();
			ImGui::Text("%.2f %.2f %.2f", m_position.x, m_position.y, m_position.z);
			ImGui::Columns(1);

			if (m_controller)
			{
				ImGui::DragFloat("Camera speed", m_controller->speed(), 0.1f, 0.1f, 20.f);
			}
		}
	}

	void Camera::recomputeMatrices()
	{
		// View matrix (created from the right, up, forward and eye position vectors)
		{
			float cosPitch = cos(m_pitch);
			float sinPitch = sin(m_pitch);
			float cosYaw = cos(m_yaw);
			float sinYaw = sin(m_yaw);

			Float3 xaxis = { cosYaw,			0,			-sinYaw };
			Float3 yaxis = { sinYaw * sinPitch, cosPitch,	cosYaw * sinPitch };
			Float3 zaxis = { sinYaw * cosPitch, -sinPitch,	cosPitch * cosYaw };

			m_viewMatrix = {
				Float4(xaxis.x, yaxis.x, zaxis.x, 0),
				Float4(xaxis.y, yaxis.y, zaxis.y, 0),
				Float4(xaxis.z, yaxis.z, zaxis.z, 0),
				Float4(-xaxis.Dot(m_position), -yaxis.Dot(m_position), -zaxis.Dot(m_position), 1)
			};
		}

		// Perspective projection matrix
		{
			float aspectRatio = m_viewportSize.x / m_viewportSize.y;
			float h = 1 / tan(mgl::DegToRad(m_verticalFov) * 0.5f);
			float w = h / aspectRatio;
			float a = m_farPlane / (m_farPlane - m_nearPlane);
			float b = (-m_nearPlane * m_farPlane) / (m_farPlane - m_nearPlane);

			m_projectionMatrix = {
				Float4(w, 0, 0, 0),
				Float4(0, h, 0, 0),
				Float4(0, 0, a, 1),
				Float4(0, 0, b, 0)
			};
		}

		// Ortographic projection matrix
		{
			static const float orthoNear = 0.f;
			static const float orthoFar = 100.f;
			float w = 2.f / m_viewportSize.x;
			float h = 2.f / m_viewportSize.y;
			float a = 1.0f / (orthoFar - orthoNear);
			float b = -a * orthoNear;

			m_orthographicMatrix = {
				Float4(w, 0, 0, 0),
				Float4(0, h, 0, 0),
				Float4(0, 0, a, 0),
				Float4(0, 0, b, 1)
			};
		}
	}
}
