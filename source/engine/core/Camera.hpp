#pragma once

#include <bx/math.h>
#include <engine/system/Errors.hpp>
#include <engine/system/DataTypes.hpp>

namespace green
{
	class ICameraController;

	struct CameraData
	{
		float projection[16];
	};

	class Camera
	{
	public:
		static const Float3 UpVector;

		Camera();

		void move(Float3 direction);
		void rotate(float yawDelta, float pitchDelta);

		void setPosition(Float3 pos);
		void setVerticalFov(float fov);
		void setViewportSize(Float2 size);
		void setNearPlane(float nearPlane);
		void setFarPlane(float farPlane);

		Float3 getPosition() const;
		Float3 getForward() const;
		Float3 getRight() const;
		float getVerticalFov() const;
		Float2 getViewportSize() const;
		float getNearPlane() const;
		float getFarPlane() const;

		const Mat& getViewMatrix() const;
		const Mat& getProjectionMatrix() const;
		const Mat& getOrthographicMatrix() const;

		void drawDebugMenu();

		void setController(ICameraController* controller) { m_controller = controller; }
		ICameraController* getController() { GR_ASSERT(m_controller, "Missing camera controller"); return m_controller; };

	private:
		void recomputeMatrices();

		ICameraController* m_controller;

		Float3 m_position;

		// TODO this stuff belongs inside the FlyCamera
		float m_yaw = 0.f;
		float m_pitch = 0.f;
		
		Mat m_viewMatrix;
		Mat m_projectionMatrix;
		Mat m_orthographicMatrix;

		float m_verticalFov = 60.f;
		float m_nearPlane = 0.1f;
		float m_farPlane = 10000.f;
		Float2 m_viewportSize = Float2(1280.f, 720.f);
	};
}
