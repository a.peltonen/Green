#pragma once

#include <engine/asset/Model.hpp>
#include <engine/asset/Material.hpp>

#include <unordered_map>
#include <string>

namespace green
{
	class Registry;
	class Database;

	struct TempStorage
	{
		// Currently this is our persistent storage for asset data like models & materials.
		std::unordered_map<std::string, std::shared_ptr<Model>>		models;
		std::unordered_map<std::string, std::shared_ptr<Material>>	materials;

		// Test scenes
		std::unordered_map<std::string, std::shared_ptr<Database>>	scenes;

	};

	class SampleScene
	{
	public:
		SampleScene(Registry& registry);

		void drawDebugMenu();

		const Database& getActiveScene() const;

	private:
		void loadAllModelsAndMaterialsTemp(const Database& s);

		int										m_selectedSceneNdx = 0;

		std::string								m_activeSceneName = "";
		TempStorage								m_storage;
	};
}