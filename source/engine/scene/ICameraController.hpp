#pragma once

namespace green
{
	class Camera;
	class InputActions;

	class ICameraController
	{
	public:
		virtual void inputUpdate(Camera& camera, const InputActions& input) = 0;

		virtual float* speed() = 0;
	};
}