#pragma once

#include <engine/scene/ICameraController.hpp>

namespace green
{
	class FlyCameraController : public ICameraController
	{
	public:
		void	inputUpdate(Camera& camera, const InputActions& input) override;
		float*	speed() override { return &m_speed; }
	private:
		float m_speed = 0.4f;
	};
}