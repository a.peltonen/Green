#include <engine/scene/FlyCameraController.hpp>
#include <engine/core/Camera.hpp>
#include <engine/core/InputActions.hpp>

namespace green
{
	void FlyCameraController::inputUpdate(Camera& camera, const InputActions& input)
	{
		float rotationSpeed = 2.5f;
		if (input.isActive(InputAction::CameraForward))
		{
			camera.move(camera.getForward() * m_speed);
		}
		if (input.isActive(InputAction::CameraBack))
		{
			camera.move(-camera.getForward() * m_speed);
		}
		if (input.isActive(InputAction::CameraRight))
		{
			camera.move(camera.getRight() * m_speed);
		}
		if (input.isActive(InputAction::CameraLeft))
		{
			camera.move(-camera.getRight() * m_speed);
		}

		if (input.getMouseRightDown() && input.mouseMoved())
		{
			Float2 screenSpaceDragDistance = input.getMouseNormalizedDelta();

			float yaw = screenSpaceDragDistance.x * rotationSpeed;		// Rotation around Y (vertical) axis (horizontal mouse delta)
			float pitch = screenSpaceDragDistance.y * rotationSpeed;	// Rotation around X (horizontal) axis (vertical mouse delta)

			camera.rotate(yaw, pitch);
		}
	}
}
