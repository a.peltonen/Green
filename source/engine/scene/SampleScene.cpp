#include <engine/scene/SampleScene.hpp>

#include <engine/ecs/Database.hpp>

#include <imgui.h>
#include <fmt/core.h>

namespace green
{

	SampleScene::SampleScene(Registry& registry)
	{
		auto fireHydrantMaterial = std::make_shared<Material>();
		fireHydrantMaterial->setSourceTextures(
			"src/materials/firehydrant/firehydrant_albedo.png",
			"src/materials/firehydrant/firehydrant_normal.png",
			"src/materials/firehydrant/firehydrant_roughness.png",
			"src/materials/firehydrant/firehydrant_metalness.png",
			"src/materials/firehydrant/firehydrant_ao.png");
		m_storage.materials["firehydrant"] = fireHydrantMaterial;

		auto chestMaterial = std::make_shared<Material>();
		chestMaterial->setSourceTextures(
			"src/materials/chest/chest_albedo.png",
			"src/materials/chest/chest_normal.png",
			"src/materials/chest/chest_roughness.png",
			"src/materials/chest/chest_metalness.png",
			"src/materials/chest/chest_ao.png");
		m_storage.materials["chest"] = chestMaterial;

		auto createModel = [&](const fs::path& path, float scale)
		{
			auto model = std::make_shared<Model>(path, scale);
			m_storage.models[model->name()] = model;
		};

		createModel("src/models/firehydrant.obj", 20.f);
		createModel("src/models/chest.obj", 40.f);
		createModel("src/models/cube.obj", 1.f);
		createModel("src/models/sphere.obj", 1.f);
		createModel("src/models/cornell_box.obj", 0.25f);
		createModel("src/models/teapot_normals.obj", 1.f);
		createModel("src/models/sponza.obj", 1.f);

		auto createEntity = [&](Database& s, const std::string& modelName, Float3 position = Float3::zero)
		{
			Id id = s.createEntity();
			s.assign<Transform>(id, Transform(position, Qua::identity, 1.0f));
			s.assign<ModelLink>(id, ModelLink(m_storage.models[modelName]));
			if (m_storage.materials.find(modelName) != m_storage.materials.end())
			{
				s.assign<MaterialLink>(id, MaterialLink(m_storage.materials[modelName]));
			}
			return id;
		};

		auto createSimpleScene = [&](const std::string& modelName)
		{
			auto db = std::make_shared<Database>(registry);
			createEntity(*db, modelName);
			m_storage.scenes[modelName] = std::move(db);
		};

		auto createGridScene = [&](const std::string& modelName, int n)
		{
			static constexpr float OffsetX = 8.f;
			static constexpr float OffsetY = 18.f;
			static constexpr float OffsetScale = 8.f;
			auto db = std::make_shared<Database>(registry);

			for (int x = -n/2; x < n/2; x++)
			{
				for (int y = -n/2; y < n/2; y++)
				{
					Id id = createEntity(*db, modelName, Float3(OffsetX+x*OffsetScale, OffsetY+y*OffsetScale, 0));

					Material::ConstantOverrides overrides;
					overrides.roughness = (x + n/2 + 0.5f) / static_cast<float>(n);
					overrides.metalness = (y + n/2 + 0.5f) / static_cast<float>(n);
					overrides.albedo = Float3(0.2, 0.2, 0.2);

					auto material = std::make_shared<Material>();
					material->setConstantOverrides(overrides);
					std::string materialName = fmt::format("{}_r{:.2f}_m{:.2f}", modelName.c_str(), overrides.roughness.value(), overrides.metalness.value());
					m_storage.materials[materialName] = material;
					db->assign<MaterialLink>(id, MaterialLink(material));
				}
			}

			m_storage.scenes[fmt::format("{}_grid", modelName.c_str())] = std::move(db);
		};

		createGridScene("sphere", 6);

		createSimpleScene("firehydrant");
		createSimpleScene("chest");
		createSimpleScene("cube");
		createSimpleScene("sphere");
		createSimpleScene("cornell_box");
		createSimpleScene("teapot_normals");
		createSimpleScene("sponza");

		m_activeSceneName = m_storage.scenes.begin()->first;

		loadAllModelsAndMaterialsTemp(getActiveScene());
	}

	void SampleScene::drawDebugMenu()
	{
		if (ImGui::CollapsingHeader("Scene"))
		{
			std::vector<const char*> sceneNames;

			for (auto&& s : m_storage.scenes)
			{
				sceneNames.emplace_back(s.first.c_str());
			}

			if (ImGui::Combo("Active scene", &m_selectedSceneNdx, sceneNames.data(), static_cast<int>(sceneNames.size())))
			{
				m_activeSceneName = sceneNames[m_selectedSceneNdx];
				loadAllModelsAndMaterialsTemp(getActiveScene());
			}
		}
	}

	const green::Database& SampleScene::getActiveScene() const
	{
		GR_ASSERT(m_storage.scenes.find(m_activeSceneName) != m_storage.scenes.end(), "Scene %s not found", m_activeSceneName.c_str());
		return *m_storage.scenes.at(m_activeSceneName);
	}

	void SampleScene::loadAllModelsAndMaterialsTemp(const Database& s)
	{
		for (auto&& id : s.allEntitiesTemp())
		{
			auto mdl = s.getComponent<ModelLink>(id);
			if (mdl && mdl->modelData && !mdl->modelData->loaded())
			{
				mdl->modelData->load();
			}
			auto mtl = s.getComponent<MaterialLink>(id);
			if (mtl && mtl->materialData && !mtl->materialData->loaded())
			{
				mtl->materialData->load();
			}
		}
	}
}