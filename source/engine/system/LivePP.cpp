#include <engine/system/LivePP.hpp>

#include <engine/system/System.hpp>
#include <engine/gfx/SceneRenderer.hpp>

#include <LivePP/API/LPP_API.h>

#include <string>

namespace green::system
{
	static bool g_compiling = false;
	static bool g_lastCompileFailed = false;

	void LivePP::init()
	{
		HMODULE livePP = lpp::lppLoadAndRegister(L"../../external/LivePP", "Green_LivePP");
		GR_ASSERT(livePP != nullptr, "Failed to load Live++ DLL");
		lpp::lppEnableAllCallingModulesSync(livePP);
		lpp::lppInstallExceptionHandler(livePP);
	}

	void LivePP::printStatus()
	{
		static int frameCounter = 0;
		frameCounter++;
		if (g_compiling)
		{
			std::string compilingText = "Recompiling";
			int anim = (frameCounter / 10) % 4;			
			for (int i = 0; i < anim; i++)
				compilingText += ".";
			bgfx::dbgTextPrintf(0, 0, 0x0a, compilingText.c_str());
		}
		if (g_lastCompileFailed)
		{
			bgfx::dbgTextPrintf(0, 0, 0x01, "Recompile failed");
		}
	}

	void onLivePPCompileStart()
	{
		GR_LOG("Recompiling source...");
		g_compiling = true;
		g_lastCompileFailed = false;
	}
	LPP_COMPILE_START_HOOK(onLivePPCompileStart);

	void onLivePPCompileSuccess()
	{
		GR_LOG("Project recompiled successfully!");
		g_compiling = false;
		g_lastCompileFailed = false;
	}
	LPP_COMPILE_SUCCESS_HOOK(onLivePPCompileSuccess);

	void onLivePPCompileError()
	{
		GR_LOG_ERROR("Project recompile failed!");
		g_compiling = false;
		g_lastCompileFailed = true;
	}
	LPP_COMPILE_ERROR_HOOK(onLivePPCompileError);
}
