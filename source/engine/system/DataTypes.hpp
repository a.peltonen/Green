#pragma once

#include <mathgeolib/Math/MathAll.h>
#include <filesystem>

namespace green
{
	struct Id
	{
		uint32_t id;

		Id()				{ id = Invalid; }
		Id(uint32_t _id)	{ id = _id; }
		~Id()				{ }

		uint32_t	operator()()					{ return id; }
		Id&			operator++()					{ ++id; return *this; }
		Id			operator++(int)					{ return id++; }
		bool		operator<(const Id& rhs) const	{ return id < rhs.id; }
		bool		operator==(const Id& rhs) const { return id == rhs.id;  }

		static constexpr uint32_t Invalid = -1;
	};

	using Qua = mgl::Quat;

	using Mat = mgl::float4x4;

	using Float2 = mgl::float2;
	using Float3 = mgl::float3;
	using Float4 = mgl::float4;

	namespace fs = std::filesystem;
}

// Inject custom hash function into std. This is required for e.g. unordered_map & unordered_set.
namespace std
{
	template <>
	struct hash<green::Id>
	{
		std::size_t operator() (const green::Id& id) const
		{
			return static_cast<size_t>(id.id);
		}
	};
}

#pragma warning(disable : 6384)
#undef ARRAY_SIZE
#define ARRAY_SIZE(a) \
  ((sizeof(a) / sizeof(*(a))) / \
	static_cast<size_t>(!(sizeof(a) % sizeof(*(a)))))