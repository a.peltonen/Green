#pragma once

#include <Windows.h>

#include <stdio.h>
#include <stdarg.h>

#define FILENAME_FROM(x)		(strrchr(x, '\\') ? strrchr(x, '\\') + 1 : x)
#define FILENAME				FILENAME_FROM(__FILE__)

#define GR_LOG(fmt, ...)			green::system::Log("%s:%d: " fmt "\n",				FILENAME, __LINE__, __VA_ARGS__)
#define GR_LOG_WARNING(fmt, ...)	green::system::Log("Warning: %s:%d: " fmt "\n",		FILENAME, __LINE__, __VA_ARGS__)
#define GR_LOG_ERROR(fmt, ...)		green::system::Log("Error: %s:%d: " fmt "\n",		FILENAME, __LINE__, __VA_ARGS__)

namespace green
{
	namespace system
	{
		static void Log(const char* fmt, ...)
		{
			const int MaxLength = 256;
			char buf[MaxLength];

			va_list args;
			va_start(args, fmt);
			vsnprintf(buf, MaxLength, fmt, args);
			va_end(args);

			// Print to console
			printf(buf);

			// Print to VS log
			OutputDebugString(buf);
		}
	}
}
