#include <engine/system/Imgui.hpp>
#include <engine/core/InputActions.hpp>
#include <engine/gfx/Common.hpp> // TODO: Illegal dependency (renderer -> system)

#include <engine/shaders/vs_imgui.bin.h>
#include <engine/shaders/fs_imgui.bin.h>

#include <imgui/imgui.h>

#include <bgfx/embedded_shader.h>

namespace green
{
	static const bgfx::EmbeddedShader s_embeddedShaders[] =
	{
		BGFX_EMBEDDED_SHADER(vs_imgui),
		BGFX_EMBEDDED_SHADER(fs_imgui),

		BGFX_EMBEDDED_SHADER_END()
	};

	bgfx::VertexLayout Imgui::s_vertexLayout;
	bgfx::TextureHandle Imgui::s_fontTexture;
	bgfx::UniformHandle Imgui::s_fontSampler;
	bgfx::ProgramHandle Imgui::s_program;

	void Imgui::init()
	{
		IMGUI_CHECKVERSION();
		ImGui::CreateContext();

		ImGuiIO& io = ImGui::GetIO();
		
		s_vertexLayout
			.begin()
			.add(bgfx::Attrib::Position, 2, bgfx::AttribType::Float)
			.add(bgfx::Attrib::TexCoord0, 2, bgfx::AttribType::Float)
			.add(bgfx::Attrib::Color0, 4, bgfx::AttribType::Uint8, true)
			.end();

		// Setup font
		unsigned char* data;
		int width, height;
		io.Fonts->AddFontDefault();
		io.Fonts->GetTexDataAsRGBA32(&data, &width, &height);
		s_fontTexture = bgfx::createTexture2D((uint16_t)width, (uint16_t)height, false, 1, bgfx::TextureFormat::BGRA8, 0, bgfx::copy(data, width * height * 4));
		s_fontSampler = bgfx::createUniform("s_tex", bgfx::UniformType::Sampler);

		// Setup shader
		bgfx::RendererType::Enum type = bgfx::getRendererType();
		bgfx::ShaderHandle vsh = bgfx::createEmbeddedShader(s_embeddedShaders, type, "vs_imgui");
		bgfx::ShaderHandle fsh = bgfx::createEmbeddedShader(s_embeddedShaders, type, "fs_imgui");
		s_program = bgfx::createProgram(vsh, fsh, true);

		// Setup style
		ImGui::StyleColorsDark();
	}

	void Imgui::updateInput(const InputActions& input, int width, int height, float deltaTimeMs)
	{
		ImGuiIO& io = ImGui::GetIO();

		io.DisplaySize = ImVec2((float)width, (float)height);
		io.DisplayFramebufferScale = ImVec2(1, 1);

		io.DeltaTime = deltaTimeMs * 1000.f;

		Float2 mouseNormalized = input.getMouseNormalizedPos();
		io.MousePos = ImVec2(mouseNormalized.x * (float)width, mouseNormalized.y * (float)height);

		io.MouseDown[0] = input.getMouseLeftDown();
		io.MouseDown[1] = input.getMouseRightDown();
		io.MouseWheel = input.getMouseWheel();

		ImGui::NewFrame();
	}

	// TODO: Should have "ImguiRenderer" under gfx?
	void Imgui::render()
	{
		ImGui::Render();

		bgfx::touch(0);

		auto drawData = ImGui::GetDrawData();

		for (int ii = 0, num = drawData->CmdListsCount; ii < num; ++ii)
		{
			bgfx::TransientVertexBuffer tvb;
			bgfx::TransientIndexBuffer tib;

			const ImDrawList* drawList = drawData->CmdLists[ii];
			uint32_t numVertices = (uint32_t)drawList->VtxBuffer.size();
			uint32_t numIndices = (uint32_t)drawList->IdxBuffer.size();

			if (!bgfx::getAvailTransientVertexBuffer(numVertices, s_vertexLayout) || !bgfx::getAvailTransientIndexBuffer(numIndices))
			{
				break;
			}

			bgfx::allocTransientVertexBuffer(&tvb, numVertices, s_vertexLayout);
			bgfx::allocTransientIndexBuffer(&tib, numIndices);

			ImDrawVert* verts = (ImDrawVert*)tvb.data;
			memcpy(verts, drawList->VtxBuffer.begin(), numVertices * sizeof(ImDrawVert));

			ImDrawIdx* indices = (ImDrawIdx*)tib.data;
			memcpy(indices, drawList->IdxBuffer.begin(), numIndices * sizeof(ImDrawIdx));

			uint32_t offset = 0;
			for (const ImDrawCmd* cmd = drawList->CmdBuffer.begin(), *cmdEnd = drawList->CmdBuffer.end(); cmd != cmdEnd; ++cmd)
			{
				if (cmd->UserCallback)
				{
					cmd->UserCallback(drawList, cmd);
				}
				else if (0 != cmd->ElemCount)
				{
					uint64_t state = BGFX_STATE_WRITE_RGB | BGFX_STATE_WRITE_A | BGFX_STATE_MSAA;
					bgfx::TextureHandle th = s_fontTexture;
					if (cmd->TextureId != NULL)
					{
						th.idx = uint16_t(uintptr_t(cmd->TextureId));
					}
					state |= BGFX_STATE_BLEND_FUNC(BGFX_STATE_BLEND_SRC_ALPHA, BGFX_STATE_BLEND_INV_SRC_ALPHA);
					const uint16_t xx = uint16_t(mgl::Max(cmd->ClipRect.x, 0.0f));
					const uint16_t yy = uint16_t(mgl::Max(cmd->ClipRect.y, 0.0f));
					bgfx::setScissor(xx, yy, uint16_t(mgl::Min(cmd->ClipRect.z, 65535.0f) - xx), uint16_t(mgl::Min(cmd->ClipRect.w, 65535.0f) - yy));
					bgfx::setState(state);
					bgfx::setTexture(0, s_fontSampler, th);
					bgfx::setVertexBuffer(0, &tvb, 0, numVertices);
					bgfx::setIndexBuffer(&tib, offset, cmd->ElemCount);
					bgfx::submit(RenderView::Finalize, s_program);
				}

				offset += cmd->ElemCount;
			}
		}
	}

	void Imgui::shutdown()
	{
		bgfx::destroy(s_fontTexture);
		bgfx::destroy(s_fontSampler);
		bgfx::destroy(s_program);
		ImGui::DestroyContext();
	}
}
