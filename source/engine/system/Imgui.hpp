#pragma once

#include <engine/system/DataTypes.hpp>
#include <bgfx/bgfx.h>
#include <windows.h>

namespace green
{
	class InputActions;

	class Imgui
	{
	public:
		static void init();
		static void updateInput(const InputActions& input, int width, int height, float dt);
		static void render();
		static void shutdown();

	private:
		static bgfx::VertexLayout s_vertexLayout;
		static bgfx::TextureHandle s_fontTexture;
		static bgfx::UniformHandle s_fontSampler;
		static bgfx::ProgramHandle s_program;
	};
}
