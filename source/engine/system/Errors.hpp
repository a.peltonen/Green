#pragma once

#include <engine/system/Logging.hpp>
#include <engine/system/pow2assert/pow2assert.h>

#define GR_ASSERT POW2_ASSERT_MSG

namespace green
{
	namespace system
	{
		pow2::Assert::FailBehavior handleAssert(
			const char* condition,
			const char* msg,
			const char* file,
			int line);
	}
}