#include <engine/system/System.hpp>

#include <engine/system/LivePP.hpp>

namespace green
{
	namespace system
	{
		void init()
		{
			// Initialize Live++
			//LivePP::init(); // Disabled due to missing license.

			// Set custom assert handler
			pow2::Assert::SetHandler(handleAssert);
		}
	}
}

