#include <engine/system/Errors.hpp>
#include <fmt/core.h>

namespace green
{
	namespace system
	{
		pow2::Assert::FailBehavior handleAssert(const char * condition, const char * msg, const char * file, int line)
		{
			std::string error = fmt::format("Assertion failed ({}:{}): '{}' {}\n", FILENAME_FROM(file), line, condition, msg);
			Log(error.c_str());
			return pow2::Assert::Halt;
		}
	}
}

