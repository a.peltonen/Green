#pragma once

#include <engine/gfx/Common.hpp>
#include <engine/system/DataTypes.hpp>

namespace green
{
	class ImageUtils
	{
	public:
		// Load environment map from a path, assuming they are stored in 6 separate per-face .hdr images.
		static bgfx::TextureHandle loadEnvmap(const fs::path& path);
		// Load texture from a file
		static bgfx::TextureHandle loadTexture(const fs::path& path, uint64_t _flags = BGFX_TEXTURE_NONE | BGFX_SAMPLER_NONE, uint8_t _skip = 0, bgfx::TextureInfo* _info = NULL);
	};
}