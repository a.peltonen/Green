#pragma once

#include <engine/gfx/utils/ImageUtils.hpp>

#include <engine/bgfx_utils/common.h>
#include <engine/bgfx_utils/bgfx_utils.h>

#include <bimg/decode.h>

namespace green
{
	bgfx::TextureHandle ImageUtils::loadEnvmap(const fs::path& path)
	{
		bgfx::TextureHandle handle = BGFX_INVALID_HANDLE;

		auto loadCubemapFace = [&handle](const char* file, uint8_t side)
		{
			uint32_t size;
			void* data = load(entry::getFileReader(), entry::getAllocator(), file, &size);
			bimg::ImageContainer* imageContainer = bimg::imageParse(entry::getAllocator(), data, size);
			const bgfx::Memory* mem = bgfx::makeRef(imageContainer->m_data, imageContainer->m_size);
			unload(data);

			if (!bgfx::isValid(handle))
			{
				handle = bgfx::createTextureCube(imageContainer->m_width, false, 1, bgfx::TextureFormat::RGBA32F);
			}

			bgfx::updateTextureCube(handle, 0, side, 0, 0, 0, imageContainer->m_width, imageContainer->m_height, mem);
		};

		loadCubemapFace((path / "nx.hdr").string().c_str(), BGFX_CUBE_MAP_NEGATIVE_X);
		loadCubemapFace((path / "ny.hdr").string().c_str(), BGFX_CUBE_MAP_NEGATIVE_Y);
		loadCubemapFace((path / "nz.hdr").string().c_str(), BGFX_CUBE_MAP_NEGATIVE_Z);
		loadCubemapFace((path / "px.hdr").string().c_str(), BGFX_CUBE_MAP_POSITIVE_X);
		loadCubemapFace((path / "py.hdr").string().c_str(), BGFX_CUBE_MAP_POSITIVE_Y);
		loadCubemapFace((path / "pz.hdr").string().c_str(), BGFX_CUBE_MAP_POSITIVE_Z);

		return handle;
	}

	bgfx::TextureHandle ImageUtils::loadTexture(const fs::path& path, uint64_t _flags, uint8_t _skip, bgfx::TextureInfo* _info)
	{
		return ::loadTexture(entry::getFileReader(), path.string().c_str(), _flags, _skip, _info, NULL);
	}
}
