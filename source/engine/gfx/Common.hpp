#pragma once

#include <bgfx/bgfx.h>
#include <engine/system/DataTypes.hpp>
#include <engine/system/System.hpp>

namespace green
{
	struct RenderView
	{
		enum RenderViewEnum : bgfx::ViewId
		{
			GeometryRender = 0,
			Lighting,
			Skybox,

			Finalize,

			Count
		};
	};

	template <typename T>
	void InitializeInvalidHandles(T* array, int size)
	{
		for (int i = 0; i < size; i++)
			array[i] = BGFX_INVALID_HANDLE;
	}
}