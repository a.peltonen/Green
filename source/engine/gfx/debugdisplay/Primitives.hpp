#pragma once

#include <engine/gfx/Common.hpp>
#include <engine/system/DataTypes.hpp>

namespace green
{
	namespace debug
	{
		class Primitive
		{
		public:
			Primitive(Float3 position);
			~Primitive();

			void    draw(bgfx::ProgramHandle program);

			void	setPosition(Float3 position)		{ m_position = position; }

			bgfx::VertexBufferHandle	getVertexBuffer() { return m_vertexBuffer; }
			bgfx::IndexBufferHandle		getIndexBuffer() { return m_indexBuffer; }

		protected:
			Float3						m_position;

			bgfx::VertexBufferHandle	m_vertexBuffer = BGFX_INVALID_HANDLE;
			bgfx::IndexBufferHandle		m_indexBuffer = BGFX_INVALID_HANDLE;
		};

		class BoxPrimitive : public Primitive
		{
		public:
			BoxPrimitive(Float3 position, Float3 extents);
			~BoxPrimitive();
		};
	}
}