#include <engine/gfx/debugdisplay/Primitives.hpp>

namespace green::debug
{

	Primitive::Primitive(Float3 position)
		: m_position(position)
	{
	}

	Primitive::~Primitive()
	{

	}

	void Primitive::draw(bgfx::ProgramHandle program)
	{
		bgfx::setVertexBuffer(0, m_vertexBuffer);
		bgfx::setIndexBuffer(m_indexBuffer);
		bgfx::setState(0
			| BGFX_STATE_WRITE_RGB
			| BGFX_STATE_WRITE_A
			| BGFX_STATE_WRITE_Z
			| BGFX_STATE_DEPTH_TEST_LESS
			| BGFX_STATE_CULL_CW
			| BGFX_STATE_PT_TRISTRIP
		);
		
		Mat transform(Qua::identity, m_position);
		transform.Transpose(); // row-major to column-major
		bgfx::setTransform(transform.v);

		bgfx::submit(RenderView::GeometryRender, program, 0, 0 | BGFX_DISCARD_INDEX_BUFFER);

		bgfx::discard();
	}

	namespace
	{
		struct PrimtiveVertexLayout
		{
			PrimtiveVertexLayout()
			{
				layout.begin()
					.add(bgfx::Attrib::Position, 3, bgfx::AttribType::Float)
					.end();
			}

			bgfx::VertexLayout layout;
		};

		static PrimtiveVertexLayout s_vertexLayout;

		static Float3 s_boxVertices[] =
		{
			{ -1.0f,  1.0f,  1.0f  },
			{ 1.0f,  1.0f,  1.0f },
			{ -1.0f, -1.0f,  1.0f },
			{ 1.0f, -1.0f,  1.0f },
			{ -1.0f,  1.0f, -1.0f },
			{ 1.0f,  1.0f, -1.0f },
			{ -1.0f, -1.0f, -1.0f },
			{ 1.0f, -1.0f, -1.0f }
		};

		static const uint16_t s_boxIndicesTriStrip[] =
		{
			0, 1, 2,
			3,
			7,
			1,
			5,
			0,
			4,
			2,
			6,
			7,
			4,
			5,
		};
	}

	BoxPrimitive::BoxPrimitive(Float3 position, Float3 extents)
		: Primitive(position)
	{
		m_vertexBuffer = bgfx::createVertexBuffer(bgfx::makeRef(s_boxVertices, sizeof(s_boxVertices)), s_vertexLayout.layout);
		m_indexBuffer = bgfx::createIndexBuffer(bgfx::makeRef(s_boxIndicesTriStrip, sizeof(s_boxIndicesTriStrip)));
	}

	BoxPrimitive::~BoxPrimitive()
	{
		bgfx::destroy(m_indexBuffer);
		bgfx::destroy(m_vertexBuffer);
	}
}
