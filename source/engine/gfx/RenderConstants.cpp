#include <engine/gfx/RenderConstants.hpp>

namespace green
{

	RenderConstants::VertexLayoutType	RenderConstants::VertexLayout;
	std::shared_ptr<Material>			RenderConstants::DefaultMaterial;


	void RenderConstants::Initialize()
	{
		VertexLayout.Model
			.begin()
			.add(bgfx::Attrib::Position, 3, bgfx::AttribType::Float)
			.add(bgfx::Attrib::Normal, 3, bgfx::AttribType::Float)
			.add(bgfx::Attrib::Tangent, 4, bgfx::AttribType::Float)
			.add(bgfx::Attrib::Color0, 4, bgfx::AttribType::Uint8, true)
			.add(bgfx::Attrib::TexCoord0, 2, bgfx::AttribType::Float)
			.end();

		VertexLayout.FullScreen
			.begin()
			.add(bgfx::Attrib::Position, 3, bgfx::AttribType::Float)
			.add(bgfx::Attrib::TexCoord0, 2, bgfx::AttribType::Float)
			.end();

		DefaultMaterial = std::make_shared<Material>();
		DefaultMaterial->setSourceTextures(
			"src/materials/default/default_albedo.png",
			"src/materials/default/default_normal.png",
			"src/materials/default/default_roughness.png",
			"src/materials/default/default_metalness.png",
			"src/materials/default/default_ao.png");
		DefaultMaterial->load();
	}

	void RenderConstants::Destroy()
	{
		DefaultMaterial.reset();
	}
}
