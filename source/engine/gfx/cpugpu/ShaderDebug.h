#if defined(__cplusplus)
#pragma once

namespace green::cpugpu
{
	static const char* DebugModeNames[] =
	{
		"None",
		"Albedo",
		"Normal",
		"Roughness",
		"Metalness",
		"AO",
	};

	static const char* NormalDebugModeNames[] =
	{
		"Normal mapped",
		"Raw vertex normal"
	};

	static const char* LightingModelNames[] =
	{
		"PBR (Cook-Torrance)",
		"Blinn-Phong"
	};

	static const char* DebugValueTempNames[] =
	{
		"None",
		"1",
		"2",
		"3",
	};
#endif

#if !defined (__cplusplus)
	// FIXME!!! 
	// Currently bgfx shader compiler doesn't notice changes to this file, figure out what's going on there.

	// NOTE: #defines because static const int doesn't work for some reason

	uniform vec4 s_debug; // x = debug mode, y = normal debug mode, z = lighting model, w = temp debug value

	#define DebugMode_None				0
	#define DebugMode_Albedo			1
	#define DebugMode_Normal			2
	#define DebugMode_Roughness			3
	#define DebugMode_Metalness			4
	#define DebugMode_AO				5

	#define NormalMode_NormalMapped		0
	#define NormalMode_RawVertexNormal	1

	#define LightingModel_PBR			0
	#define LightingModel_BlinnPhong	1

	#define DebugValue_Temp_None		0
	#define DebugValue_Temp_1			1
	#define DebugValue_Temp_2			2
	#define DebugValue_Temp_3			3

	bool IsDebugModeActive(int debugMode)
	{
		return abs(float(debugMode) - s_debug.x) < 0.1;
	}

	bool IsNormalDebugModeActive(int debugMode)
	{
		return abs(float(debugMode) - s_debug.y) < 0.1;
	}

	bool IsCurrentLightingModel(int lightingModel)
	{
		return abs(float(lightingModel) - s_debug.z) < 0.1;
	}

	bool IsDebugValueTemp(int value)
	{
		return abs(float(value) - s_debug.w) < 0.1;
	}

#endif

#if defined(__cplusplus)
}
#endif