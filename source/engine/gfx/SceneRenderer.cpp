#include <engine/gfx/SceneRenderer.hpp>

#include <engine/gfx/RenderConstants.hpp>
#include <engine/asset/Model.hpp>
#include <engine/asset/Material.hpp>
#include <engine/gfx/utils/ImageUtils.hpp>

#include <bgfx/embedded_shader.h>

#include <engine/shaders/vs_geometry.bin.h>
#include <engine/shaders/fs_geometry.bin.h>
#include <engine/shaders/vs_lighting.bin.h>
#include <engine/shaders/fs_lighting.bin.h>
#include <engine/shaders/vs_skybox.bin.h>
#include <engine/shaders/fs_skybox.bin.h>
#include <engine/shaders/vs_finalize.bin.h>
#include <engine/shaders/fs_finalize.bin.h>

#include <engine/gfx/cpugpu/ShaderDebug.h>

#include <engine/ecs/Database.hpp>
#include <engine/ecs/Component.hpp>

#include <imgui.h>

namespace green
{
	// TODO: Move into a class
	static fs::path s_envmapPaths[] =
	{
		"src/envmaps/barcelona/",
		"src/envmaps/zion/"
	};
	static const char* s_envmapNames[] =
	{
		"Barcelona",
		"Zion"
	};
	static_assert(ARRAY_SIZE(s_envmapPaths) == ARRAY_SIZE(s_envmapNames));

	// TODO: Remove embedded shaders
	static const bgfx::EmbeddedShader s_embeddedShaders[] =
	{
		BGFX_EMBEDDED_SHADER(vs_geometry),
		BGFX_EMBEDDED_SHADER(fs_geometry),
		BGFX_EMBEDDED_SHADER(vs_lighting),
		BGFX_EMBEDDED_SHADER(fs_lighting),
		BGFX_EMBEDDED_SHADER(vs_skybox),
		BGFX_EMBEDDED_SHADER(fs_skybox),
		BGFX_EMBEDDED_SHADER(vs_finalize),
		BGFX_EMBEDDED_SHADER(fs_finalize),

		BGFX_EMBEDDED_SHADER_END()
	};

	SceneRenderer::SceneRenderer()
		: m_lightPos(10.f, 30.f, -5.f)
		, m_lightDebugBox(m_lightPos, Float3(1,1,1))
		, m_lightColor(0.90f, 0.82f, 0.78f)
		, m_lightIntensity(600.f)
	{
		InitializeInvalidHandles(m_gbufferSamplers, ARRAY_SIZE(m_gbufferSamplers));
		InitializeInvalidHandles(m_textureSamplers, ARRAY_SIZE(m_textureSamplers));

		RenderConstants::Initialize();

		bgfx::RendererType::Enum type = bgfx::getRendererType();

		// Geometry pass shader creation

		bgfx::ShaderHandle vsh = bgfx::createEmbeddedShader(s_embeddedShaders, type, "vs_geometry");
		bgfx::ShaderHandle fsh = bgfx::createEmbeddedShader(s_embeddedShaders, type, "fs_geometry");

		m_textureSamplers[0] = bgfx::createUniform("s_tAlbedo",		bgfx::UniformType::Sampler);
		m_textureSamplers[1] = bgfx::createUniform("s_tNormal",		bgfx::UniformType::Sampler);
		m_textureSamplers[2] = bgfx::createUniform("s_tRoughness",	bgfx::UniformType::Sampler);
		m_textureSamplers[3] = bgfx::createUniform("s_tMetalness",	bgfx::UniformType::Sampler);
		m_textureSamplers[4] = bgfx::createUniform("s_tAO",			bgfx::UniformType::Sampler);

		m_overrideAlbedoUniform = bgfx::createUniform("s_overrideAlbedo", bgfx::UniformType::Vec4);
		m_overrideRoughnessUniform = bgfx::createUniform("s_overrideRoughness", bgfx::UniformType::Vec4);
		m_overrideMetalnessUniform = bgfx::createUniform("s_overrideMetalness", bgfx::UniformType::Vec4);

		m_geometryPassProgram = bgfx::createProgram(vsh, fsh, true);

		// Gbuffer pass shader creation

		bgfx::ShaderHandle vshFullscreen = bgfx::createEmbeddedShader(s_embeddedShaders, type, "vs_lighting");
		bgfx::ShaderHandle fshFullscreen = bgfx::createEmbeddedShader(s_embeddedShaders, type, "fs_lighting");
		m_lightingPassProgram		= bgfx::createProgram(vshFullscreen, fshFullscreen, true);
		m_gbufferSamplers[0]		= bgfx::createUniform("s_gbuffer0", bgfx::UniformType::Sampler);
		m_gbufferSamplers[1]		= bgfx::createUniform("s_gbuffer1", bgfx::UniformType::Sampler);
		m_gbufferSamplers[2]		= bgfx::createUniform("s_gbuffer2", bgfx::UniformType::Sampler);
		m_gbufferSamplers[3]		= bgfx::createUniform("s_gbufferZ", bgfx::UniformType::Sampler);
		m_envmapTextureUniform		= bgfx::createUniform("s_envmapDiffuse", bgfx::UniformType::Sampler);
		m_lightPosUniform			= bgfx::createUniform("s_lightPos", bgfx::UniformType::Vec4);
		m_lightColorUniform			= bgfx::createUniform("s_lightColor", bgfx::UniformType::Vec4);
		m_viewPosUniform			= bgfx::createUniform("s_viewPos", bgfx::UniformType::Vec4);
		m_shaderDebugModeUniform	= bgfx::createUniform("s_debug", bgfx::UniformType::Vec4);

		// Skybox pass shader creation

		bgfx::ShaderHandle vshSkybox = bgfx::createEmbeddedShader(s_embeddedShaders, type, "vs_skybox");
		bgfx::ShaderHandle fshSkybox = bgfx::createEmbeddedShader(s_embeddedShaders, type, "fs_skybox");
		m_skyboxProgram = bgfx::createProgram(vshSkybox, fshSkybox, true);
		m_skyboxCubeUniform = bgfx::createUniform("s_skybox", bgfx::UniformType::Sampler);

		// Finalize pass shader creation

		bgfx::ShaderHandle vshFinalize = bgfx::createEmbeddedShader(s_embeddedShaders, type, "vs_finalize");
		bgfx::ShaderHandle fshFinalize = bgfx::createEmbeddedShader(s_embeddedShaders, type, "fs_finalize");
		m_finalizeProgram = bgfx::createProgram(vshFinalize, fshFinalize, true);
		m_finalizeSamplerUniform = bgfx::createUniform("s_texture", bgfx::UniformType::Sampler);

		// Load envmaps
		for (int i = 0; i < ARRAY_SIZE(s_envmapPaths); i++)
		{
			m_envMaps[i].original	= ImageUtils::loadEnvmap(s_envmapPaths[i] / "original");
			m_envMaps[i].convolved	= ImageUtils::loadEnvmap(s_envmapPaths[i] / "convolved");
			m_envMaps[i].preview	= ImageUtils::loadTexture(s_envmapPaths[i] / "original/preview.jpg");
		}
	}

	SceneRenderer::~SceneRenderer()
	{
		bgfx::destroy(m_geometryPassProgram);

		RenderConstants::Destroy();
	}

	void SceneRenderer::drawDebugMenu()
	{
		if (ImGui::CollapsingHeader("SceneRenderer"))
		{
			// TODO: Move into components
			ImGui::DragFloat3("Light position", m_lightPos.ptr());
			ImGui::ColorEdit3("Light color", m_lightColor.ptr());
			ImGui::DragFloat("Light intensity", &m_lightIntensity);

			ImGui::Separator();
	
			ImGui::Combo("Environment map (diffuse)", &m_selectedEnvMapIndex, s_envmapNames, ARRAY_SIZE(s_envmapNames));

			if (bgfx::isValid(m_envMaps[m_selectedEnvMapIndex].preview))
			{
				ImGui::Image(reinterpret_cast<void*>(m_envMaps[m_selectedEnvMapIndex].preview.idx), ImVec2(200, 100));
			}

			ImGui::Separator();
						
			ImGui::Combo("Lighting model",			&m_shaderLightingModel,		cpugpu::LightingModelNames,		ARRAY_SIZE(cpugpu::LightingModelNames));
			ImGui::Combo("Gbuffer debug",			&m_shaderDebugMode,			cpugpu::DebugModeNames,			ARRAY_SIZE(cpugpu::DebugModeNames));
			ImGui::Combo("Normal mode",				&m_shaderNormalDebugMode,	cpugpu::NormalDebugModeNames,	ARRAY_SIZE(cpugpu::NormalDebugModeNames));
			ImGui::Combo("Shader debug temp value", &m_shaderDebugTempValue,	cpugpu::DebugValueTempNames,	ARRAY_SIZE(cpugpu::DebugValueTempNames));

			ImGui::Separator();

			if (ImGui::CollapsingHeader("Gbuffer"))
			{
				ImGui::Text("Layer"); ImGui::SameLine();
				ImGui::RadioButton("1", &m_showGbufferlayer, 0); ImGui::SameLine();
				ImGui::RadioButton("2", &m_showGbufferlayer, 1); ImGui::SameLine();
				ImGui::RadioButton("3", &m_showGbufferlayer, 2); ImGui::SameLine();
				ImGui::RadioButton("4", &m_showGbufferlayer, 3);
				float w = ImGui::CalcItemWidth();
				float h = (9.f / 16.f) * w;
				ImGui::Image(reinterpret_cast<void*>(m_renderCtx.gbuffer[m_showGbufferlayer].idx), ImVec2(w, h));
			}
		}
	}

	void SceneRenderer::update(const Camera& camera)
	{
		static float minx = 0.f;
		static float maxx = 0.f;
		static float miny = 0.f;
		static float maxy = 0.f;
		static const float z = 0.f;
		static const float minu = 0.f;
		static const float minv = 1.f;
		static const float maxu = 1.f;
		static const float maxv = 0.f;

		/*if (windowWidth != m_renderCtx.width || windowHeight != m_renderCtx.height)
		{
			m_renderCtx.resize(windowWidth, windowHeight);
		}*/
		Float2 viewportSize = camera.getViewportSize();
		if (viewportSize.x/2.f != maxx || viewportSize.y/2.f != maxy)
		{
			m_renderCtx.resize(static_cast<int>(viewportSize.x), static_cast<int>(viewportSize.y));

			minx = -viewportSize.x	/ 2.f;
			miny = -viewportSize.y	/ 2.f;
			maxx = viewportSize.x	/ 2.f;
			maxy = viewportSize.y	/ 2.f;

			{
				float vertexBufferData[] = {
					minx, miny, z, minu, minv, // Bottom-right	uv = [0, 1]
					maxx, miny, z, maxu, minv, // Bottom-left	uv = [1, 1]
					maxx, maxy, z, maxu, maxv, // Top-left		uv = [1, 0]
					minx, maxy, z, minu, maxv  // Top-right		uv = [0, 0]
				};
			
				auto mem = bgfx::alloc(sizeof(vertexBufferData));
				bx::memMove(mem->data, &vertexBufferData[0], mem->size);
				m_fullScreenPassVertexBuffer = bgfx::createVertexBuffer(mem, RenderConstants::VertexLayout.FullScreen);
			}
			{
				uint16_t indexBufferData[] = {
					0, 2, 1,
					0, 3, 2
				};

				auto mem = bgfx::alloc(sizeof(indexBufferData));
				bx::memMove(mem->data, &indexBufferData[0], mem->size);
				m_fullScreenPassIndexBuffer = bgfx::createIndexBuffer(mem);
			}
		}
	}

	void SceneRenderer::render(const Database& db, const Camera& camera)
	{
		configureRenderViews(camera);

		for (auto&& id : db.allEntitiesWithComponentTemp<ModelLink>())
		{
			// Geometry pass
			auto model = db.getComponent<ModelLink>(id);
			auto material = db.getComponent<MaterialLink>(id);
			auto transform = db.getComponent<Transform>(id);

			if (model)
				renderModel(m_geometryPassProgram, 
					model->modelData.get(), 
					material ? material->materialData.get() : nullptr,
					transform ? *transform : Transform());
		}

		// TEMP: Render light position
		m_lightDebugBox.setPosition(m_lightPos);
		m_lightDebugBox.draw(m_geometryPassProgram); // FIXME: Use separate debug draw shader - this is not properly configured and debug layer doesn't like it

		renderLightingPass(camera.getPosition());

		renderSkybox();
		
		finalizeBackBuffer();
	}

	void SceneRenderer::configureRenderViews(const Camera& camera)
	{
		// MathGeoLib is row major and bgfx is column major - convert only when these two interact.
		const Mat viewMatrixColumnMajor		= camera.getViewMatrix().Transposed();
		const Mat projMatrixColumnMajor		= camera.getProjectionMatrix().Transposed();
		const Mat orthoMatrixColumnMajor	= camera.getOrthographicMatrix().Transposed();
		const Float2 viewportSize			= camera.getViewportSize();

		// Geometry pass
		bgfx::setViewFrameBuffer(RenderView::GeometryRender, m_renderCtx.gbufferFbo); // Draw to gbuffer FBO
		bgfx::setViewTransform(RenderView::GeometryRender, viewMatrixColumnMajor.v, projMatrixColumnMajor.v);
		bgfx::setViewRect(RenderView::GeometryRender, 0, 0, uint16_t(viewportSize.x), uint16_t(viewportSize.y));
		bgfx::setViewName(RenderView::GeometryRender, "Geometry render view");
		bgfx::setViewClear(RenderView::GeometryRender, BGFX_CLEAR_COLOR | BGFX_CLEAR_DEPTH, 0x303030ff, 1.0f, 0);

		// Lighting pass
		bgfx::setViewFrameBuffer(RenderView::Lighting, m_renderCtx.postfxFbo); // Draw to postfx FBO
		bgfx::setViewTransform(RenderView::Lighting, NULL, orthoMatrixColumnMajor.v);
		bgfx::setViewRect(RenderView::Lighting, 0, 0, uint16_t(viewportSize.x), uint16_t(viewportSize.y));
		bgfx::setViewName(RenderView::Lighting, "Gbuffer render view");
		bgfx::setViewClear(RenderView::Lighting, BGFX_CLEAR_COLOR, 0x303030ff, 1.0f, 0); // Keep depth information from geometry pass

		// Skybox pass
		bgfx::setViewFrameBuffer(RenderView::Skybox, m_renderCtx.postfxFbo); // Draw to postfx FBO
		bgfx::setViewTransform(RenderView::Skybox, viewMatrixColumnMajor.v, projMatrixColumnMajor.v);
		bgfx::setViewRect(RenderView::Skybox, 0, 0, uint16_t(viewportSize.x), uint16_t(viewportSize.y));
		bgfx::setViewName(RenderView::Skybox, "Skybox render view");
		bgfx::setViewClear(RenderView::Skybox, BGFX_CLEAR_NONE); // Rendering on top of lighting result, don't clear color or depth

		// Back buffer finalize pass
		bgfx::setViewFrameBuffer(RenderView::Finalize, BGFX_INVALID_HANDLE); // Draw to back buffer
		bgfx::setViewTransform(RenderView::Finalize, NULL, orthoMatrixColumnMajor.v);
		bgfx::setViewRect(RenderView::Finalize, 0, 0, uint16_t(viewportSize.x), uint16_t(viewportSize.y));
		bgfx::setViewName(RenderView::Finalize, "Finalize render view");
		bgfx::setViewClear(RenderView::Finalize, BGFX_CLEAR_COLOR | BGFX_CLEAR_DEPTH, 0x303030ff, 1.0f, 0);
	}

	void SceneRenderer::renderModel(bgfx::ProgramHandle program, const Model* model, const Material* material, Transform transform)
	{
		if (!model->loaded())
		{
			GR_LOG_ERROR("Trying to render model %s that is not loaded", model->name().c_str());
			return;
		}

		Mat transformMat = Mat::FromTRS(transform.translation, transform.rotation, Float3(transform.scale));
		transformMat.Transpose(); // row-major to column-major
		bgfx::setTransform(transformMat.v);		

		bgfx::setState(0
			| BGFX_STATE_WRITE_RGB
			| BGFX_STATE_WRITE_A
			| BGFX_STATE_WRITE_Z
			| BGFX_STATE_DEPTH_TEST_LESS
			| BGFX_STATE_CULL_CCW
			//| BGFX_STATE_PT_TRISTRIP
		);

		auto defaultMaterial = RenderConstants::DefaultMaterial.get();
		if (!material)
		{
			material = defaultMaterial;
		}

		setConstantMaterialParameterOverrides(material);

		for (int i = 0; i < 5; i++)
			if (bgfx::isValid(material->runtimeData().textures[i]))
				bgfx::setTexture(i, m_textureSamplers[i], material->runtimeData().textures[i]);
			else
				bgfx::setTexture(i, m_textureSamplers[i], defaultMaterial->runtimeData().textures[i]);

		for (auto&& sm : model->runtimeData().submodels)
		{
			bgfx::setVertexBuffer(0, sm.vertexBuffer);
			bgfx::setIndexBuffer(sm.indexBuffer);
			bgfx::submit(RenderView::GeometryRender, program, 0, 0 | BGFX_DISCARD_INDEX_BUFFER | BGFX_DISCARD_VERTEX_STREAMS);
		}

		bgfx::discard();
	}

	void SceneRenderer::renderLightingPass(Float3 cameraPos)
	{
		bgfx::setState(BGFX_STATE_WRITE_RGB	| BGFX_STATE_WRITE_A);
		bgfx::setIndexBuffer(m_fullScreenPassIndexBuffer);
		bgfx::setVertexBuffer(0, m_fullScreenPassVertexBuffer);
		bgfx::setTexture(0, m_gbufferSamplers[0], m_renderCtx.gbuffer[0]);
		bgfx::setTexture(1, m_gbufferSamplers[1], m_renderCtx.gbuffer[1]);
		bgfx::setTexture(2, m_gbufferSamplers[2], m_renderCtx.gbuffer[2]);
		bgfx::setTexture(3, m_envmapTextureUniform, m_envMaps[m_selectedEnvMapIndex].convolved);
		float lightPos[4] = { m_lightPos.x, m_lightPos.y, m_lightPos.z, 1.f };
		bgfx::setUniform(m_lightPosUniform, lightPos);
		float lightColor[4] = { m_lightColor.x * m_lightIntensity, m_lightColor.y * m_lightIntensity, m_lightColor.z * m_lightIntensity, 1.f };
		bgfx::setUniform(m_lightColorUniform, lightColor);
		float viewPos[4] = { cameraPos.x, cameraPos.y, cameraPos.z, 1.f };
		bgfx::setUniform(m_viewPosUniform, viewPos);
		float debugModes[4] = { static_cast<float>(m_shaderDebugMode), static_cast<float>(m_shaderNormalDebugMode), static_cast<float>(m_shaderLightingModel), static_cast<float>(m_shaderDebugTempValue) };
		bgfx::setUniform(m_shaderDebugModeUniform, debugModes);

		bgfx::submit(RenderView::Lighting, m_lightingPassProgram);

		bgfx::discard();
	}

	void SceneRenderer::renderSkybox()
	{
		debug::BoxPrimitive skybox(Float3::zero, Float3::one);

		bgfx::setVertexBuffer(0, skybox.getVertexBuffer());
		bgfx::setIndexBuffer(skybox.getIndexBuffer());
		bgfx::setState(0
			| BGFX_STATE_WRITE_RGB
			| BGFX_STATE_WRITE_A
			| BGFX_STATE_DEPTH_TEST_LEQUAL // z=1 trick
			| BGFX_STATE_CULL_CCW
			| BGFX_STATE_PT_TRISTRIP
		);

		bgfx::setTexture(0, m_skyboxCubeUniform, m_envMaps[m_selectedEnvMapIndex].original);

		Mat transformMat = Mat::FromTRS(Float3::zero, Qua::identity, Float3(10000.f));
		transformMat.Transpose(); // row-major to column-major
		bgfx::setTransform(transformMat.v);

		bgfx::submit(RenderView::Skybox, m_skyboxProgram, 0, 0 | BGFX_DISCARD_INDEX_BUFFER);

		bgfx::discard();
	}

	void SceneRenderer::setConstantMaterialParameterOverrides(const Material* material)
	{
		static const Float4 NoValue(-1, -1, -1, -1);		

		const auto& overrides = material->getConstantOverrides();
		if (overrides.albedo)
		{
			float v[4] = { overrides.albedo->x, overrides.albedo->y, overrides.albedo->z, 1.f };
			bgfx::setUniform(m_overrideAlbedoUniform, v);
		}
		else
		{
			bgfx::setUniform(m_overrideAlbedoUniform, &NoValue);
		}

		if (overrides.roughness)
		{
			float v[4] = { overrides.roughness.value(), 0.f, 0.f, 0.f };
			bgfx::setUniform(m_overrideRoughnessUniform, v);
		}
		else
		{
			bgfx::setUniform(m_overrideRoughnessUniform, &NoValue);
		}

		if (overrides.metalness)
		{
			float v[4] = { overrides.metalness.value(), 0.f, 0.f, 0.f };
			bgfx::setUniform(m_overrideMetalnessUniform, v);
		}
		else
		{
			bgfx::setUniform(m_overrideMetalnessUniform, &NoValue);
		}
	}

	void SceneRenderer::finalizeBackBuffer()
	{
		bgfx::setState(BGFX_STATE_WRITE_RGB);
		bgfx::setIndexBuffer(m_fullScreenPassIndexBuffer);
		bgfx::setVertexBuffer(0, m_fullScreenPassVertexBuffer);
		bgfx::setTexture(0, m_finalizeSamplerUniform, m_renderCtx.postfxTexture);

		bgfx::submit(RenderView::Finalize, m_finalizeProgram);

		bgfx::discard();
	}

}
