#pragma once

#include <engine/gfx/Common.hpp>
#include <engine/gfx/RenderContext.hpp>
#include <engine/gfx/debugdisplay/Primitives.hpp>
#include <engine/core/Camera.hpp>

namespace green
{
	class Database;
	class Model;
	class Material;
	struct Transform;

	class SceneRenderer
	{
	public:
		SceneRenderer();
		~SceneRenderer();

		void	update(const Camera& camera);
		void    render(const Database& db, const Camera& camera);

		void	drawDebugMenu();

	private:
		void	configureRenderViews(const Camera& camera);

		void    renderModel(bgfx::ProgramHandle program, const Model* model, const Material* material, Transform transform);
		void	renderLightingPass(Float3 cameraPos);
		void	renderSkybox();
		void	finalizeBackBuffer();

		void	setConstantMaterialParameterOverrides(const Material* material);

		// Geometry pass
		RenderContext							m_renderCtx;

		bgfx::ProgramHandle						m_geometryPassProgram = BGFX_INVALID_HANDLE;

		bgfx::UniformHandle						m_gbufferSamplers[4];
		bgfx::UniformHandle						m_textureSamplers[5];

		bgfx::UniformHandle						m_overrideAlbedoUniform = BGFX_INVALID_HANDLE;
		bgfx::UniformHandle						m_overrideRoughnessUniform = BGFX_INVALID_HANDLE;
		bgfx::UniformHandle						m_overrideMetalnessUniform = BGFX_INVALID_HANDLE;

		// Gbuffer pass
		bgfx::ProgramHandle						m_lightingPassProgram = BGFX_INVALID_HANDLE;
		bgfx::VertexBufferHandle				m_fullScreenPassVertexBuffer = BGFX_INVALID_HANDLE;
		bgfx::IndexBufferHandle					m_fullScreenPassIndexBuffer = BGFX_INVALID_HANDLE;
		bgfx::UniformHandle						m_envmapTextureUniform = BGFX_INVALID_HANDLE;
		bgfx::UniformHandle						m_lightPosUniform = BGFX_INVALID_HANDLE;
		bgfx::UniformHandle						m_lightColorUniform = BGFX_INVALID_HANDLE;
		bgfx::UniformHandle						m_viewPosUniform = BGFX_INVALID_HANDLE;

		// Skybox pass
		bgfx::ProgramHandle						m_skyboxProgram = BGFX_INVALID_HANDLE;
		bgfx::UniformHandle						m_skyboxCubeUniform = BGFX_INVALID_HANDLE;

		// Finalize pass
		bgfx::ProgramHandle						m_finalizeProgram = BGFX_INVALID_HANDLE;
		bgfx::UniformHandle						m_finalizeSamplerUniform = BGFX_INVALID_HANDLE;

		// Environment maps
		struct EnvMap
		{
			bgfx::TextureHandle original	= BGFX_INVALID_HANDLE;
			bgfx::TextureHandle convolved	= BGFX_INVALID_HANDLE;
			bgfx::TextureHandle preview		= BGFX_INVALID_HANDLE;
		};
		EnvMap									m_envMaps[2];
		int										m_selectedEnvMapIndex = 0;

		Float3									m_lightPos;
		Float3									m_lightColor;
		float									m_lightIntensity;
		debug::BoxPrimitive						m_lightDebugBox;

		bgfx::UniformHandle						m_shaderDebugModeUniform = BGFX_INVALID_HANDLE;
		int										m_shaderDebugMode = 0;
		int										m_shaderNormalDebugMode = 0;
		int										m_shaderLightingModel = 0;
		int										m_shaderDebugTempValue = 0;
		int										m_showGbufferlayer = 0;
	};
}