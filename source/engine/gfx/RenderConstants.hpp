#pragma once

#include <engine/gfx/Common.hpp>
#include <engine/asset/Material.hpp>

namespace green
{
	class RenderConstants
	{
		friend class Model;
		friend class SceneRenderer;

	private:
		static void Initialize();
		static void Destroy();

		static struct VertexLayoutType
		{
			bgfx::VertexLayout Model;
			bgfx::VertexLayout FullScreen;
		} VertexLayout;

		static std::shared_ptr<Material> DefaultMaterial;
	};
}