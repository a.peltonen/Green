#pragma once

#include <engine/gfx/Common.hpp>

namespace green
{
	struct RenderContext
	{
	public:
		RenderContext() {};

		void resize(int width, int height)
		{
			const uint64_t flags = 0
				| BGFX_SAMPLER_MIN_POINT
				| BGFX_SAMPLER_MAG_POINT
				| BGFX_SAMPLER_MIP_POINT
				| BGFX_TEXTURE_RT;

			{
				gbuffer[0] = bgfx::createTexture2D(width, height, false, 1, bgfx::TextureFormat::RGBA32F, flags);	// XYZ worldPos, W roughness.	// TODO: Reconstruct world position from depth
				gbuffer[1] = bgfx::createTexture2D(width, height, false, 1, bgfx::TextureFormat::RGBA32F, flags);	// XYZ normal, W metalness.		// TODO: Pack normal using octahedral encoding or similar
				gbuffer[2] = bgfx::createTexture2D(width, height, false, 1, bgfx::TextureFormat::RGBA32F, flags);	// RGB albedo, W ao.
				gbuffer[3] = bgfx::createTexture2D(width, height, false, 1, bgfx::TextureFormat::D24S8, flags);

				bgfx::Attachment attachments[4];
				attachments[0].init(gbuffer[0], bgfx::Access::Write);
				attachments[1].init(gbuffer[1], bgfx::Access::Write);
				attachments[2].init(gbuffer[2], bgfx::Access::Write);
				attachments[3].init(gbuffer[3], bgfx::Access::Write);

				gbufferFbo = bgfx::createFrameBuffer(4, attachments, true);
			}
			
			{
				postfxTexture = bgfx::createTexture2D(width, height, false, 1, bgfx::TextureFormat::RGBA8, flags);

				bgfx::Attachment attachments[2];
				attachments[0].init(postfxTexture, bgfx::Access::Write);
				attachments[1].init(gbuffer[3], bgfx::Access::Read);

				postfxFbo = bgfx::createFrameBuffer(2, attachments);
			}
		}

		bgfx::TextureHandle			gbuffer[4] = { BGFX_INVALID_HANDLE };
		bgfx::FrameBufferHandle		gbufferFbo = BGFX_INVALID_HANDLE;

		bgfx::TextureHandle			postfxTexture = BGFX_INVALID_HANDLE;
		bgfx::FrameBufferHandle		postfxFbo = BGFX_INVALID_HANDLE;

		int width = 0;
		int height = 0;
	};
}