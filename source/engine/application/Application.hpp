#pragma once

#include <engine/bgfx_utils/common.h>
#include <engine/bgfx_utils/bgfx_utils.h>
#include <engine/bgfx_utils/entry/input.h>

#include <engine/core/InputActions.hpp>
#include <engine/core/Camera.hpp>
#include <engine/scene/FlyCameraController.hpp>

#include <memory>
#include <vector>
#include <unordered_set>

namespace green
{
	class SceneRenderer;
	class Registry;
	class Database;
	class SampleScene;
}

class Application : public entry::AppI
{
public:
	Application(const char* _name, const char* _description, const char* _url);
	~Application();

	virtual void		init(int32_t _argc, const char* const* _argv, uint32_t _width, uint32_t _height) override;
	virtual int			shutdown() override;

	bool				update() override;

private:
	void				drawDebugMenu();
	uint32_t			makeDebugFlags();
	uint32_t			makeResetFlags();

	green::FlyCameraController					m_cameraController; // TODO: Store somewhere else (in a component?)
	green::Camera								m_camera;			// TODO: Store in a component
	green::InputActions							m_inputActions;
	std::unique_ptr<green::Registry>			m_registry = nullptr;
	std::unique_ptr<green::SceneRenderer>		m_sceneRenderer = nullptr;
	std::unique_ptr<green::SampleScene>			m_sampleScene = nullptr;

	uint32_t                        m_width;
	uint32_t                        m_height;
	int64_t                         m_applicationStartTime;

	// Bgfx config settings
	struct
	{
		bool stats = false;
		bool wireframe = false;
	} m_debugFlags;

	struct
	{
		bool vsync = true;
	} m_resetFlags;

	double							m_deltaTimeMs;
	double							m_elapsedTimeMs;
};
