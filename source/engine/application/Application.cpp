#include <engine/application/Application.hpp>

#include <engine/system/System.hpp>
#include <engine/system/LivePP.hpp>
#include <engine/system/Imgui.hpp>
#include <engine/gfx/SceneRenderer.hpp>
#include <engine/core/Camera.hpp>
#include <engine/scene/SampleScene.hpp>
#include <engine/ecs/Registry.hpp>
#include <engine/ecs/Database.hpp>

#include <imgui.h>

#include <filesystem>

Application::Application(const char* _name, const char* _description, const char* _url)
	: entry::AppI(_name, _description, _url)
{
	// Set current working directory
	green::fs::current_path("../../data/");
}

Application::~Application()
{
}

void Application::init(int32_t _argc, const char* const* _argv, uint32_t _width, uint32_t _height)
{
	using namespace green;

	Args args(_argc, _argv);

	m_width = _width;
	m_height = _height;

	system::init();

	bgfx::Init initData;
	initData.type = args.m_type;
	initData.vendorId = args.m_pciId;
	bgfx::init(initData);
	bgfx::reset(m_width, m_height, makeResetFlags());
	bgfx::setDebug(makeDebugFlags());

	m_sceneRenderer = std::make_unique<SceneRenderer>();

	m_registry = std::make_unique<Registry>();
	m_registry->registerComponentType<Transform>();
	m_registry->registerComponentType<ModelLink>();
	m_registry->registerComponentType<MaterialLink>();

	m_sampleScene = std::make_unique<SampleScene>(*m_registry);

	Imgui::init();

	m_applicationStartTime = bx::getHPCounter();

	// temp
	m_camera.setController(&m_cameraController);
}

int Application::shutdown()
{
	using namespace green;

	m_sceneRenderer = nullptr;

	Imgui::shutdown();
	bgfx::shutdown();

	return 0;
}
		
bool Application::update()
{
	using namespace green;

	uint32_t resetFlags = makeResetFlags();
	uint32_t debugFlags = makeDebugFlags();
	bool shouldExit = entry::processEvents(m_width, m_height, debugFlags, resetFlags);

	if (!shouldExit)
	{
		m_camera.setViewportSize(Float2(static_cast<float>(m_width), static_cast<float>(m_height)));

		// TODO: Move to "time.cpp", make static global
		int64_t currentFrameTicks = bx::getHPCounter();
		static int64_t lastFrameTicks = currentFrameTicks;
		const int64_t deltaTimeTicks = currentFrameTicks - lastFrameTicks;
		lastFrameTicks = currentFrameTicks;
		const double toMs = 1000.0 / double(bx::getHPFrequency());
		m_deltaTimeMs = (double)deltaTimeTicks * toMs;
		m_elapsedTimeMs = (double)(currentFrameTicks - m_applicationStartTime) * toMs;

		// Print LivePP compilation status
		system::LivePP::printStatus();

		// Process & react to input
		m_inputActions.update();

		// Imgui input
		Imgui::updateInput(m_inputActions, m_width, m_height, (float)m_deltaTimeMs);

		m_camera.getController()->inputUpdate(m_camera, m_inputActions);

		m_sceneRenderer->update(m_camera);
		m_sceneRenderer->render(m_sampleScene->getActiveScene(), m_camera);
		
		drawDebugMenu();

		Imgui::render();

		bgfx::frame();
	}

	return !shouldExit;
}

void Application::drawDebugMenu()
{
	// Style
	ImGui::GetStyle().WindowRounding = 0;

	// Frametime overlay
	// TODO: Move to time.cpp
	ImGui::SetNextWindowPos(ImVec2(5.f, 5.f));
	ImGui::SetNextWindowSize(ImVec2(180.f, 45.f));
	ImGui::SetNextWindowBgAlpha(0.25f);
	ImGuiWindowFlags flags = 
		ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoSavedSettings |
		ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav | ImGuiWindowFlags_NoMove;

	if (ImGui::Begin("FPS overlay", nullptr, flags))
	{
		ImGui::Columns(2, "overlaycolumns", false);
		ImGui::Text("Frametime"); ImGui::NextColumn(); ImGui::Text("%7.2f ms", m_deltaTimeMs); ImGui::NextColumn();
		ImGui::Text("FPS"); ImGui::NextColumn(); ImGui::Text("%7.2f", 1000.0f/m_deltaTimeMs); ImGui::NextColumn();
		ImGui::Columns(1);
		ImGui::End();
	}

	static bool drawDebugMenu = true;
	if (m_inputActions.isActive(green::InputAction::ToggleDebugMenu))
	{
		drawDebugMenu = !drawDebugMenu;
	}
	if (!drawDebugMenu)
	{
		return;
	}

	// Debug menu
	ImGui::SetNextWindowPos(ImVec2(5.f, 55.f), ImGuiCond_FirstUseEver);
	ImGui::SetNextWindowSize(ImVec2(400.f, 300.f), ImGuiCond_FirstUseEver);

	static bool open = false;
	ImGui::Begin("Debug menu", &open);

	static bool demo = false;
	ImGui::Checkbox("Imgui demo", &demo);
	if (demo)
	{
		ImGui::ShowDemoWindow();
	}

	if (ImGui::Checkbox("Bgfx stats", &m_debugFlags.stats) ||
		ImGui::Checkbox("Wireframe", &m_debugFlags.wireframe))
	{
		bgfx::setDebug(makeDebugFlags());
	}

	if (ImGui::Checkbox("Vsync", &m_resetFlags.vsync))
	{
		bgfx::reset(m_width, m_height, makeResetFlags());
	}

	m_camera.drawDebugMenu();
	m_inputActions.drawDebugMenu();
	m_sampleScene->drawDebugMenu();
	m_sceneRenderer->drawDebugMenu();
	
	ImGui::End();
}

uint32_t Application::makeDebugFlags()
{
	uint32_t debugFlags = BGFX_DEBUG_NONE;
	debugFlags |= m_debugFlags.stats		? BGFX_DEBUG_STATS		: 0;
	debugFlags |= m_debugFlags.wireframe	? BGFX_DEBUG_WIREFRAME	: 0;
	return debugFlags;
}

uint32_t Application::makeResetFlags()
{
	uint32_t resetFlags = BGFX_RESET_NONE;
	resetFlags |= m_resetFlags.vsync ? BGFX_RESET_VSYNC : 0;
	return resetFlags;
}
