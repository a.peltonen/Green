$input v_texcoord0

#include "../bgfx_utils/bgfx_shader.sh"
#include "../gfx/cpugpu/ShaderDebug.h"
#include "math.sh"

SAMPLER2D(s_gbuffer0, 0);
SAMPLER2D(s_gbuffer1, 1);
SAMPLER2D(s_gbuffer2, 2);
SAMPLERCUBE(s_envmapDiffuse, 3);

uniform vec4 s_lightPos;
uniform vec4 s_lightColor;
uniform vec4 s_viewPos;

#define PI 3.1415926535897932384626433

// Simple Blinn-Phong
vec3 BlinnPhong(vec3 albedo, vec3 normal, vec3 worldPos)
{
    vec3 viewDir = normalize(s_viewPos.xyz - worldPos);

    vec3 color = albedo * 0.2; // "ambient term"

    // for each light...
    {
        vec3 lightDir = normalize(vec3(s_lightPos.x, s_lightPos.y, s_lightPos.z) - worldPos);
                        
        vec3 diffuse = saturate(dot(normal, lightDir)) * albedo;
        
        const float Shininess = 60.0;
        vec3 halfVector = normalize(lightDir + viewDir);
        float specular = pow(saturate(dot(normal, halfVector)), Shininess);

        color += diffuse + vec3_splat(specular);
        
        float distance      = length(s_lightPos.xyz - worldPos);
        float attenuation   = 1.0 / (distance * distance);
        color *= s_lightColor.xyz * attenuation;
    }

    return color;
}

// Cook-Torrance PBR implemented based on following sources:
// https://blog.selfshadow.com/publications/s2013-shading-course/hoffman/s2013_pbs_physics_math_notes.pdf
// https://learnopengl.com/PBR/Lighting
// https://mynameismjp.wordpress.com/2016/10/09/sg-series-part-4-specular-lighting-from-an-sg-light-source/
// http://www.codinglabs.net/article_physically_based_rendering_cook_torrance.aspx

// https://www.desmos.com/calculator/u5unsfrcbe
vec3 FresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

// "Fudged Fresnel attenuation" to avoid glowing edges
// https://seblagarde.wordpress.com/2011/08/17/hello-world/
vec3 FresnelSchlickWithRoughness(float cosTheta, vec3 F0, float roughness)
{
    return F0 + (max(vec3_splat(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
} 

float TrowbridgeReitzGGX(vec3 N, vec3 H, float roughness)
{
    float a      = roughness*roughness;
    float a2     = a*a;
    float NdotH  = saturate(dot(N, H));
    float NdotH2 = NdotH*NdotH;
	
    float num   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;
	
    return num / denom;
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float num   = NdotV;
    float denom = NdotV * (1.0 - k) + k;
	
    return num / denom;
}

float GeometrySmith(float NdotV, float NdotL, float roughness)
{
    float ggx2  = GeometrySchlickGGX(NdotV, roughness);
    float ggx1  = GeometrySchlickGGX(NdotL, roughness);
	
    return ggx1 * ggx2;
}

void main()
{
	vec4 gbuffer0 = texture2D(s_gbuffer0, v_texcoord0);
	vec4 gbuffer1 = texture2D(s_gbuffer1, v_texcoord0);
	vec4 gbuffer2 = texture2D(s_gbuffer2, v_texcoord0);

    vec3 worldPos       = gbuffer0.xyz;
    vec3 normal         = gbuffer1.xyz;
    vec3 albedo         = gbuffer2.rgb;
    float roughness     = gbuffer0.w;
    float metalness     = gbuffer1.w;
    float ao            = gbuffer2.w;

    vec3 color;
    if (IsCurrentLightingModel(LightingModel_BlinnPhong))
    {
        color = BlinnPhong(albedo, normal, worldPos);
    }
    else
    {
        vec3 N = normalize(normal);                     // Surface normal
        vec3 V = normalize(s_viewPos.xyz - worldPos);   // View vector
        float NdotV = saturate(dot(N, V));
        vec3 F0 = vec3_splat(0.04);                     // Reflectivity F0 = 0.04 (dielectric materials)
        F0 = mix(F0, albedo, metalness);
           
        vec3 totalRadiance = vec3_splat(0.0);
        // for each light...
        {
            vec3 L              = normalize(s_lightPos.xyz - worldPos);  // Light direction vetor
            vec3 H              = normalize(V + L);                      // Half-vector between light and view  
            float distance      = length(s_lightPos.xyz - worldPos);
            float attenuation   = 1.0 / (distance * distance);
            vec3 radiance       = s_lightColor.xyz * attenuation;
            
            float NdotL = saturate(dot(N, L));
            float HdotV = saturate(dot(H, V));

            // Cook-Torrance BRDF

            float D = TrowbridgeReitzGGX(N, H, roughness);          // Normal distribution function; models "microgeometry bumpiness"
            float G = GeometrySmith(NdotV, NdotL, roughness);       // Geometry term; models local occlusion
            vec3  F = FresnelSchlick(HdotV, F0);                    // Fresnel term; determines amount of reflected light (vs. refracted and absorbed)
                    
            vec3 DFG = D * G * F;                                   // Total amount of reflected light
            vec3 specular = DFG / max(4.0 * NdotV * NdotL, 0.001);  // Divide by "Correction factor" to get final specular value
            
            vec3 kD = vec3_splat(1.0) - F;                          // Amount of light that is not reflected, i.e. it is refracted --> "diffuse part"
            kD *= 1.0 - metalness;                                  // Remove diffuse from metal materials
            vec3 diffuse = kD * albedo / PI;                        // Integrate diffuse over the hemisphere

            totalRadiance += (diffuse + specular) * radiance * NdotL; 
        }
        
        // Calculate ambient term from convolved diffuse environment map
        vec3 kS = FresnelSchlickWithRoughness(NdotV, F0, roughness);
        vec3 kD = 1.0 - kS;
        vec3 envmapIrradiance = textureCube(s_envmapDiffuse, N).rgb;
        vec3 envmapDiffuse = envmapIrradiance * albedo;
        vec3 envmapAmbient = (kD * envmapDiffuse) * ao;

        color = envmapAmbient + totalRadiance;    
    }
    
    if (IsDebugModeActive(DebugMode_Albedo))
    {
        color = albedo;
    }
    else if (IsDebugModeActive(DebugMode_Normal))
    {
        color = normal;
    }
    else if (IsDebugModeActive(DebugMode_Roughness))
    {
        color = vec3_splat(roughness);
    }
    else if (IsDebugModeActive(DebugMode_Metalness))
    {
        color = vec3_splat(metalness);
    }
    else if (IsDebugModeActive(DebugMode_AO))
    {
        color = vec3_splat(ao);
    }
    
    if (IsDebugModeActive(DebugMode_None))
    {
        // Gamma conversion
        color = ToGammaSpace(color);
    }

	gl_FragColor = vec4(color.r, color.g, color.b, 1); 
}
