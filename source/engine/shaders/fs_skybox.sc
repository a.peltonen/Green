$input v_localPos

#include "../bgfx_utils/bgfx_shader.sh"
#include "math.sh"

SAMPLERCUBE(s_skybox, 0);

void main()
{
	vec3 color = textureCube(s_skybox, v_localPos).rgb;
	color = ToGammaSpace(color);

	gl_FragColor = vec4(color.r, color.g, color.b, 1.0);
}
