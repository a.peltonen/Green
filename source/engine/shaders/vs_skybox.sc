$input a_position
$output v_localPos

#include "../bgfx_utils/bgfx_shader.sh"

void main()
{
	v_localPos = a_position;

    vec4 clipPos = mul(u_modelViewProj, vec4(a_position, 1.0));
	gl_Position = clipPos.xyww; // Force Z = 1 (after w divide), meaning will be behind everything
}
