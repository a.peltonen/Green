$input v_texcoord0

#include "../bgfx_utils/bgfx_shader.sh"

SAMPLER2D(s_texture, 0);

void main()
{
	vec3 color = texture2D(s_texture, v_texcoord0).rgb;
	gl_FragColor = vec4(color.r, color.g, color.b, 1.0);
}
