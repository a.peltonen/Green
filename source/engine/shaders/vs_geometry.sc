$input a_position, a_normal, a_tangent, a_bitangent, a_color0, a_texcoord0
$output v_color0, v_worldPos, v_normal, v_tangent, v_bitangent, v_texcoord0

#include "../bgfx_utils/common.sh"

void main()
{
	gl_Position = mul(u_modelViewProj, vec4(a_position, 1.0));
	v_worldPos	= mul(u_model[0], vec4(a_position, 1.0)).xyz;
	v_color0	= a_color0;
	v_texcoord0 = a_texcoord0;
	
	vec3 normal			= a_normal;
	vec3 tangent		= a_tangent.xyz;
	float bitangentSign = a_tangent.w;
	vec3 bitangent		= bitangentSign * cross(normal, tangent);
	
	mat3 normalMatrix = cofactor(u_model[0]);
	v_normal	= mul(normalMatrix, normal);
	v_tangent	= mul(normalMatrix, tangent);
	v_bitangent = mul(normalMatrix, bitangent);
}
