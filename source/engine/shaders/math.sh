
vec3 ToGammaSpace(vec3 color)
{
	const float InvGamma = 1.0f / 2.2f;
    color = color / (color + vec3_splat(1.0));
    color = pow(color, vec3_splat(InvGamma));
    return color;
}