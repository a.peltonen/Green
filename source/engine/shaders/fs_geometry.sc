$input v_color0, v_worldPos, v_normal, v_tangent, v_bitangent, v_texcoord0
 
#include "../bgfx_utils/common.sh"
#include "../gfx/cpugpu/ShaderDebug.h"

SAMPLER2D(s_tAlbedo,	0);
SAMPLER2D(s_tNormal,	1);
SAMPLER2D(s_tRoughness, 2);
SAMPLER2D(s_tMetalness,	3);
SAMPLER2D(s_tAO,		4);

uniform vec4 s_overrideAlbedo;
uniform vec4 s_overrideRoughness;
uniform vec4 s_overrideMetalness;

void main()
{
	vec3 albedo = texture2D(s_tAlbedo, v_texcoord0).rgb;
	
	mat3 tbn = transpose(mat3(v_tangent, v_bitangent, v_normal));
	vec3 normal;
	normal.xy = texture2D(s_tNormal, v_texcoord0).xy * 2.0 - 1.0;
	normal.z = sqrt(1.0 - dot(normal.xy, normal.xy));
	normal = normalize(mul(tbn, normal));
	
	float roughness = texture2D(s_tRoughness, v_texcoord0).r;
	float metalness = texture2D(s_tMetalness, v_texcoord0).r;
	float ao		= texture2D(s_tAO, v_texcoord0).r;
	
	// TEMP: Hack to display something when textures are missing
	if (length(albedo) == 0.0)
		albedo = vec3_splat(0.5);
		
	// Material parameter constant overrides
	if (s_overrideAlbedo.x >= 0.0)
		albedo = s_overrideAlbedo.rgb;
	if (s_overrideRoughness.x >= 0.0)
		roughness = s_overrideRoughness.r;
	if (s_overrideMetalness.x >= 0.0)
		metalness = s_overrideMetalness.r;
			
	if (IsNormalDebugModeActive(NormalMode_RawVertexNormal))
	{
		normal = normalize(v_normal);
	}

	gl_FragData[0] = vec4(v_worldPos.x, v_worldPos.y, v_worldPos.z, roughness);
	gl_FragData[1] = vec4(normal.x, normal.y, normal.z, metalness);
	gl_FragData[2] = vec4(albedo.r, albedo.g, albedo.b, ao);
}
