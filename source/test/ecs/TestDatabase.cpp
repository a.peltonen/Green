#include <test/TestCommon.hpp>

#include <engine/ecs/Database.hpp>
#include <engine/ecs/Component.hpp>

using namespace green;

struct ComponentA : public system::Component
{
    int i;

    ComponentA(int v) : i(v) {}
    bool operator==(const ComponentA& rhs) const { return i == rhs.i; }
};

struct ComponentB : public system::Component
{
    float f;

    ComponentB(float v) : f(v) {}
    bool operator==(const ComponentB& rhs) const { return f == rhs.f; }
};

TEST_CASE_METHOD(GRTest, "ecs.Database.Basic", "[Scene]")
{
    Registry reg;
    reg.registerComponentType<ComponentA>();
    reg.registerComponentType<ComponentB>();
    Database db(reg);

    SECTION("Generic")
    {
        Id eId1 = db.createEntity();
        Id eId2 = db.createEntity();
        Id eId3 = db.createEntity();

        SECTION("Entity ids")
        {
            auto all = db.allEntitiesTemp();
            REQUIRE(all.size() == 0); // No tables registered - empty
        }
    }

    SECTION("Component operations")
    {
        ComponentA originalA(42);
        ComponentB originalB(99.f);

        Id tId1 = db.createEntity();
        Id tId2 = db.createEntity();
        Id tId3 = db.createEntity();

        SECTION("Copy assignment lvalue")
        {
            ComponentA c = originalA;
            db.assign(tId1, c);
            REQUIRE(*db.getComponent<ComponentA>(tId1) == originalA);
        }

        SECTION("Copy assignment rvalue")
        {
            db.assign(tId1, ComponentA(originalA));
            REQUIRE(*db.getComponent<ComponentA>(tId1) == originalA);

        }

        SECTION("Move assignment")
        {
            ComponentA c = originalA;
            db.assign(tId1, std::move(c));
            REQUIRE(*db.getComponent<ComponentA>(tId1) == ComponentA(originalA));
        }

        SECTION("Retrieve")
        {
            db.assign(tId1, originalA);

            auto components = db.allEntitiesWithComponentTemp<ComponentA>();
            REQUIRE(components.size() == 1);
            REQUIRE(components[0] == tId1);
            REQUIRE(*db.getComponent<ComponentA>(tId1) == originalA);

            db.assign(tId2, originalB);
            auto all = db.allEntitiesTemp();
            REQUIRE(all.size() == 2);
            REQUIRE(all[1] == tId2);
            REQUIRE(*db.getComponent<ComponentB>(tId2) == originalB);
        }

        SECTION("Remove")
        {
            db.assign(tId1, originalA);
            auto components = db.allEntitiesWithComponentTemp<ComponentA>();
            REQUIRE(components.size() == 1);
            db.remove<ComponentA>(tId1);
            components = db.allEntitiesWithComponentTemp<ComponentA>();
            REQUIRE(components.size() == 0);
            auto all = db.allEntitiesTemp();
            REQUIRE(all.size() == 0);
        }

        SECTION("Clone")
        {
            auto clone1 = Database(reg);
            REQUIRE(clone1.allEntitiesTemp().size() == 0);
            Id cloneId1 = clone1.createEntity();
            REQUIRE(cloneId1 == tId1);
        }
    }
}