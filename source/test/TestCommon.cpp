#include <test/TestCommon.hpp>

pow2::Assert::FailBehavior handleTestAssert(const char* condition, const char* msg, const char* file, int line)
{
    std::string error = fmt::format("Assertion failed ({}:{}): '{}' {}\n", FILENAME_FROM(file), line, condition, msg);
    green::system::Log(error.c_str());
    throw std::logic_error(error);
}