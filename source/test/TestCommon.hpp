#pragma once

#include <catch2/catch.hpp>
#include <engine/system/Errors.hpp>
#include <fmt/core.h>

pow2::Assert::FailBehavior handleTestAssert(const char* condition, const char* msg, const char* file, int line);

// Test fixture that configures asserts to throw, so catch2 can detect them
class GRTest
{
public:
    GRTest()
    {
        pow2::Assert::SetHandler(handleTestAssert);
    }
};