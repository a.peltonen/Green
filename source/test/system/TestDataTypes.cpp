#include <test/TestCommon.hpp>

#include <engine/system/DataTypes.hpp>
#include <unordered_map>

using namespace green;

TEST_CASE_METHOD(GRTest, "system.DataTypes.Id", "[System]")
{
    Id a = Id(123);
    REQUIRE(a.id == 123);

    SECTION("Prefix & postfix addition operators")
    {
        CHECK(a++ == 123);
        CHECK(a == 124);
        CHECK(++a == 125);
    }

    SECTION("Usage in std::unordered_map")
    {
        std::unordered_map<Id, Id> map;
        map[a] = a;
        CHECK(map[a] == a);
    }
}