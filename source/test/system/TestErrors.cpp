#include <test/TestCommon.hpp>

#include <engine/system/Errors.hpp>

using namespace green;

TEST_CASE_METHOD(GRTest, "system.Errors.Asserts", "[System]")
{
    auto fnThatDoesntAssert = [&]()
    {
        GR_ASSERT(true, "no assert here");
    };

    auto fnThatAsserts = [&]()
    {
        GR_ASSERT(false, "assert here");
    };

    REQUIRE_NOTHROW(fnThatDoesntAssert());
    REQUIRE_THROWS(fnThatAsserts());
}