# Green #

## What is it? ##

Green is a personal project that started out as a prototype for hosting a C++ 3D engine in a separate C#/WPF application process. Since the prototype was verified as working, most of the effort has gone into developing a simple 3D renderer. In the long term, it's intended to serve as a test platform for testing different rendering algorithms and features. 

Some currently supported features
- Model loading, FPS camera navigation
- Deferred rendering
- Basic lighting: Blinn-Phong & Cook-Torrance (PBR with image-based lighting)
- Imgui integration, debug modes for gbuffer, lighting etc.

![](screenshot.png)

## How do I get set up? ##

1. Run generate_projects.bat to generate the Visual Studio 2019 project (platform toolset v142, Windows Kit 10.0.18362.0).
2. Run compile_all.bat to compile Debug and Release targets.


## Libraries used ##

Rather than "rolling my own everything", the strategy has been to reuse as much as possible. Here's a non-exhaustive list.
- **BGFX** for graphics API abstraction & application skeleton (application & input intended to be removed)
- **Assimp** for model loading
- **Mathgeolib** for math types
- **Dear imgui** for debug UI
- **Live++** for runtime recompilation
- **Sharpmake** for the generating projects.