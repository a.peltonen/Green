@echo off

setlocal
SET PATH=%PATH%;%~dp0tools\bgfx\linuxtools

setlocal enabledelayedexpansion

pushd tools
for /f "usebackq tokens=*" %%i in (`vswhere -latest -requires Microsoft.Component.MSBuild -find MSBuild\**\Bin\MSBuild.exe`) do (
  set MSBUILD_EXE=%%i
)
popd

set MSBUILD_TARGET=Build

for %%a in (%*) do (
  if %%a == -clean set MSBUILD_TARGET=Clean;Rebuild
)

echo.
echo *** Building Debug ***
echo.
"%MSBUILD_EXE%" projects/green.sln -target:%MSBUILD_TARGET% /p:configuration=Debug
echo.
echo *** Building Release ***
echo.
"%MSBUILD_EXE%" projects/green.sln -target:%MSBUILD_TARGET% /p:configuration=Release
echo.
echo *** Building Shaders ***
echo.
./compile_shaders.bat