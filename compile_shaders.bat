@echo off

setlocal
SET PATH=%PATH%;%~dp0tools\bgfx\linuxtools

cd /D %~dp0/source/engine/shaders
make -f _compile_shaders.mk all
